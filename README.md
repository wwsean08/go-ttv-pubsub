# Twitch.tv pubsub client
[![pipeline status](https://gitlab.com/wwsean08/go-ttv-pubsub/badges/master/pipeline.svg)](https://gitlab.com/wwsean08/go-ttv-pubsub/-/commits/master) [![coverage report](https://gitlab.com/wwsean08/go-ttv-pubsub/badges/master/coverage.svg)](https://gitlab.com/wwsean08/go-ttv-pubsub/-/commits/master) [![go.dev reference](https://img.shields.io/badge/go.dev-reference-007d9c?logo=go&logoColor=white&style=flat)](https://pkg.go.dev/gitlab.com/wwsean08/go-ttv-pubsub)

## Author
This fork is maintained by Sean Smith, but is based on the repo originally created by [Lauri Orgla](https://github.com/theorx/go-ttv-pubsub)

## Description
*PUBSUB Websocket api client implementation written in golang, motivation was to build a client
that actually works. There are other implementations that are not working so well or are not 
working at all. This implementation covers all publicly documented behaviours of the pubsub api.*

* Documentation to official twitch api [Twitch pubsub doc](https://dev.twitch.tv/docs/pubsub)

## Connection handling
*Twitch requires to send a __PING__ message at least once per 5 minutes. This implementation is
sending a __PING__ every 60 seconds.*

*Twitch states that every ping request __should receive a response within 10 seconds__, this 
implementation will wait __up to 20 seconds__ before going to a __reconnect state__*

*In case of __RECONNECT__ or failing to receive a __PONG__ all the current topics
that you have subscribed to __will get resubscribed automatically__*

__For example:__

```go

//you create a client c
c.Subscribe(..)
//now RECONNECT happens
//you will not need to call Subscribe again, the topics will be subscribed to
//on the new connection automatically

```

## API Overview

* Set\*Handler functions are for setting handler functions to the types of topics you have subscribed to

```go

	client.SetCatchAllHandler(func(message ttvclient.IncomingMessage) {})
	client.SetLogFunction(func(i ...interface{}) {})
	client.SetBitsHandler(func(message ttvclient.BitsMsg) {})
	client.SetModerationHandler(func(message ttvclient.ModerationActionMsg) {})
	client.SetCommerceHandler(func(message ttvclient.CommerceMsg) {})
	client.SetBitsBadgeHandler(func(message ttvclient.BitsBadgeMsg) {})
	client.SetSubscriptionsHandler(func(message ttvclient.SubscriptionMsg) {})
	client.SetWhisperHandler(func(message ttvclient.WhisperMsg) {})
	client.SetChannelPointRedemptionHandler(func(message ttvclient.ChannelPointRedemptionMsg) {})
	client.SetVideoPlaybackByIDHandler(func(message ttvclint.VideoPlaybackMsg) {})
	client.SetChannelPointEarnedHandler(func(message ttvclient.ChannelPointEarnedMsg) {})
	client.SetPollHandler(func(message ttvclient.PollMsg) {})
	client.SetBroadcastSettingsUpdateHandler(func(message ttvclient.BroadcastSettingsUpdateMsg) {})
	client.SetUnknownHandler(func(message ttvclient.IncomingMessage) {})
```

* CreateClient creates the client, expects access token and twitch websocket endpoint
* There is one predefined constant: __ttvclient.TwitchPubSubHost__ which corresponds to: wss://pubsub-edge.twitch.tv/
* Endpoint required for __CreateClient__ requires correct prefix `wss://`
```go

	ttvclient.CreateClient("your-access_token", TTVClient.TwitchPubSubHost)

```

* Subscribe/Unsubscribe have the same signature
* Both of the operations require a slice of Topics - `[]topic.Topic`

```go
	client.Subscribe([]Topic.Topic{
		topic.Bits(1),
		topic.BitsBadgeNotification(1),
		topic.Commerce(1),
		topic.Whispers(1),
		topic.Subscriptions(1),
		topic.ModerationAction(1, 2),
		topic.ChannelPointsRedemption(1),
		topic.VideoPlaybackByID(1),
		topic.ChannelPointsEarned(1),
		topic.Polls(1),
		topic.BroadcasterSettingsUpdate(1),
	})

	client.Unsubscribe([]Topic.Topic{
		topic.Bits(1),
		topic.BitsBadgeNotification(1),
		topic.Commerce(1),
		topic.Whispers(1),
		topic.Subscriptions(1),
		topic.ModerationAction(1, 2),
		topic.ChannelPointsRedemption(1),
		topic.VideoPlaybackByID(1),
		topic.ChannelPointsEarned(1),
		topic.Polls(1),
		topic.BroadcasterSettingsUpdate(1),
	})

```

* topic.Bits(`channel id`)
* topic.BitsBadgeNotification(`channel id`)
* topic.Commerce(`channel id`)
* topic.Whispers(`user id`)
* topic.Subscriptions(`channel id`)
* topic.ModerationAction(`user id`, `channel id`)
* topic.ChannelPointsRedemption(`channel id`)
* topic.VideoPlaybackByID(`channel id`)
* topic.ChannelPointsEarned(`user id`)
* topic.Polls(`channel id`)
* topic.BroadcasterSettingsUpdate(`channel id`)

* Closing the connection - `client.Close()`

### Undocumented Topics

The following topics are undocumented by twitch and may change or go away at any time:

| Topic                   | Required Scope |
|-------------------------|----------------|
| VideoPlaybackByID       | None           |
| ChannelPointsEarned     | [^1]Unknown    |
| Polls                   | None           |
| BroadcastSettingsUpdate | None           |

[^1]: While I can get it to work using the auth token the browser uses, I am unable to get it to work with any of the scopes associated with it (that I can actually add)

### Errors

* ttvclient.ErrorOperationFailed = errors.New("sub/unsub operation failed") <- In this case, usually the credentials are incorrect
* ttvclient.ErrorNotConnected = errors.New("not connected") <- Connection is down / TTVClient has been closed


## Usage example

```go

package main

import (
	"gitlab.com/wwsean08/go-ttv-pubsub/pkg/ttvclient"
	"gitlab.com/wwsean08/go-ttv-pubsub/pkg/topic"
	"log"
	"time"
)

func main() {

	//create a connection
	client, err := ttvclient.CreateClient("your-access_token", ttvclient.TwitchPubSubHost)

	if err != nil {
		log.Fatal("Failed to connect", err)
		return
	}

	//set up handlers before subscribing

	client.SetModerationHandler(func(message ttvclient.ModerationActionMsg) {
		log.Println("Moderation event received", message)
	})

	err = client.Subscribe(
		[]topic.Topic{
			topic.ModerationAction(64417816, 64417816),
		},
	)

	if err != nil {
		log.Println(err)
		return
	}

	time.Sleep(time.Second * 60)

	log.Println(client.Close())
}

```

## Footnotes
