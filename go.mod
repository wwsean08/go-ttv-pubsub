module gitlab.com/wwsean08/go-ttv-pubsub

go 1.13

require (
	github.com/google/uuid v1.1.1
	github.com/gorilla/websocket v1.4.1
	github.com/stretchr/testify v1.5.1
)
