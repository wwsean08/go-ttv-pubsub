package topic

import (
	"github.com/stretchr/testify/require"
	"testing"
)

func TestBits(t *testing.T) {
	bitsTopic := Bits(12345)
	require.Equal(t, "channel-bits-events-v2.12345", string(bitsTopic))
}

func TestBitsBadgeNotification(t *testing.T) {
	bitsBadgeTopic := BitsBadgeNotification(12345)
	require.Equal(t, "channel-bits-badge-unlocks.12345", string(bitsBadgeTopic))
}

func TestSubscriptions(t *testing.T) {
	subscriptionTopic := Subscriptions(12345)
	require.Equal(t, "channel-subscribe-events-v1.12345", string(subscriptionTopic))
}

func TestCommerce(t *testing.T) {
	commerceTopic := Commerce(12345)
	require.Equal(t, "channel-commerce-events-v1.12345", string(commerceTopic))
}

func TestWhispers(t *testing.T) {
	whisperTopic := Whispers(12345)
	require.Equal(t, "whispers.12345", string(whisperTopic))
}

func TestModerationAction(t *testing.T) {
	modActionTopic := ModerationAction(12345, 67890)
	require.Equal(t, "chat_moderator_actions.12345.67890", string(modActionTopic))
}

func TestChannelPointsRedemption(t *testing.T) {
	redemption := ChannelPointsRedemption(12345)
	require.Equal(t, "channel-points-channel-v1.12345", string(redemption))
}

func TestVideoPlaybackByID(t *testing.T) {
	videoPlayback := VideoPlaybackByID(12345)
	require.Equal(t, "video-playback-by-id.12345", string(videoPlayback))
}

func TestChannelPointsEarned(t *testing.T) {
	earned := ChannelPointsEarned(12345)
	require.Equal(t, "community-points-user-v1.12345", string(earned))
}

func TestPolls(t *testing.T) {
	polls := Polls(12345)
	require.Equal(t, "polls.12345", string(polls))
}

func TestBroadcasterSettingsUpdate(t *testing.T) {
	topic := BroadcasterSettingsUpdate(12345)
	require.Equal(t, "broadcast-settings-update.12345", string(topic))
}
