package topic_test

import (
	"github.com/stretchr/testify/require"
	"gitlab.com/wwsean08/go-ttv-pubsub/pkg/topic"
	"testing"
)

func TestGetType(t *testing.T) {

	type TestCase struct {
		Type  topic.Type
		Input string
	}

	table := []TestCase{
		{topic.TypeInvalid, "test"},
		{topic.TypeInvalid, ""},
		{topic.TypeInvalid, "wispers.44322889"}, // the typo should cause type invalid
		{topic.TypeBits, "channel-bits-events-v2.46024993"},
		{topic.TypeWhispers, "whispers.44322889"},
		{topic.TypeCommerce, "channel-commerce-events-v1.44322889"},
		{topic.TypeSubscriptions, "channel-subscribe-events-v1.44322889"},
		{topic.TypeBitsBadgeNotification, "channel-bits-badge-unlocks.44322889"},
		{topic.TypeModerationAction, "chat_moderator_actions.test.test"},
		{topic.TypeChannelPointRedemption, "channel-points-channel-v1.44322889"},
		{topic.TypeVideoPlaybackByID, "video-playback-by-id.12345"},
		{topic.TypeChannelPointEarned, "community-points-user-v1.47073625"},
		{topic.TypePolls, "polls.123456"},
		{topic.TypeBroadcasterSettingsUpdate, "broadcast-settings-update.12345"},
	}

	for _, test := range table {
		result := topic.GetType(test.Input)
		require.IsType(t, test.Type, result, "Failed getting type expected '%v' but got '%v'", result, test.Type)
	}
}
