package topic

import "fmt"

// Topic provides an easy way to differentiate between topic types when subscribing
type Topic string

// Bits returns a bits topic for the given channel ID.
func Bits(channelID int) Topic {
	return Topic(fmt.Sprintf("channel-bits-events-v2.%d", channelID))
}

// BitsBadgeNotification returns a bits badge notification topic for the given channel ID.
func BitsBadgeNotification(channelID int) Topic {
	return Topic(fmt.Sprintf("channel-bits-badge-unlocks.%d", channelID))
}

// Subscriptions returns a subscription topic for the given channel ID.
func Subscriptions(channelID int) Topic {
	return Topic(fmt.Sprintf("channel-subscribe-events-v1.%d", channelID))
}

// Commerce returns a commerce topic for the given channel ID.
//
// Deprecated: this topic is deprecated by twitch with no replacement announced.
func Commerce(channelID int) Topic {
	return Topic(fmt.Sprintf("channel-commerce-events-v1.%d", channelID))
}

// Whispers returns a whisper topic for the given channel ID.
func Whispers(channelID int) Topic {
	return Topic(fmt.Sprintf("whispers.%d", channelID))
}

// ModerationAction returns a mod action topic for the given user ID and channel ID.
//
// Note: This is not an officially documented topic from twitch so could change or go away at any time.
func ModerationAction(userID int, channelID int) Topic {
	return Topic(fmt.Sprintf("chat_moderator_actions.%d.%d", userID, channelID))
}

// ChannelPointsRedemption returns a channel points redemption topic for the given channel ID.
func ChannelPointsRedemption(channelID int) Topic {
	return Topic(fmt.Sprintf("channel-points-channel-v1.%d", channelID))
}

// VideoPlaybackByID returns a video playback by id topic for the given channel id.
func VideoPlaybackByID(channelID int) Topic {
	return Topic(fmt.Sprintf("video-playback-by-id.%d", channelID))
}

// ChannelPointsEarned returns a topic for listening for channel points on a user
func ChannelPointsEarned(userID int) Topic {
	return Topic(fmt.Sprintf("community-points-user-v1.%d", userID))
}

// Polls returns a poll topic for the channel to listen to updates regarding polls
func Polls(channelID int) Topic {
	return Topic(fmt.Sprintf("polls.%d", channelID))
}

// BroadcasterSettingsUpdate returns a topic for listening to updates regarding stream settings
func BroadcasterSettingsUpdate(channelID int) Topic {
	return Topic(fmt.Sprintf("broadcast-settings-update.%d", channelID))
}
