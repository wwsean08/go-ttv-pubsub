package topic

import "strings"

// Type provides a way to differentiate between different types of messages
type Type string

// const All types
const (
	TypeBits                      = Type("channel-bits-events-v2")
	TypeBitsBadgeNotification     = Type("channel-bits-badge-unlocks")
	TypeSubscriptions             = Type("channel-subscribe-events-v1")
	TypeCommerce                  = Type("channel-commerce-events-v1")
	TypeWhispers                  = Type("whispers")
	TypeModerationAction          = Type("chat_moderator_actions")
	TypeChannelPointRedemption    = Type("channel-points-channel-v1")
	TypeVideoPlaybackByID         = Type("video-playback-by-id")
	TypeChannelPointEarned        = Type("community-points-user-v1")
	TypePolls                     = Type("polls")
	TypeBroadcasterSettingsUpdate = Type("broadcast-settings-update")
	TypeInvalid                   = Type("invalid")
	TypePong                      = Type("PONG")
	TypeReconnect                 = Type("RECONNECT")
)

// GetType takes a topic string and returns what Type it is
func GetType(topic string) Type {
	pieces := strings.Split(topic, ".")

	if len(pieces) < 2 {
		return TypeInvalid
	}

	switch Type(pieces[0]) {
	case TypeBits:
		return TypeBits
	case TypeBitsBadgeNotification:
		return TypeBitsBadgeNotification
	case TypeSubscriptions:
		return TypeSubscriptions
	case TypeCommerce:
		return TypeCommerce
	case TypeWhispers:
		return TypeWhispers
	case TypeModerationAction:
		return TypeModerationAction
	case TypeChannelPointRedemption:
		return TypeChannelPointRedemption
	case TypeVideoPlaybackByID:
		return TypeVideoPlaybackByID
	case TypeChannelPointEarned:
		return TypeChannelPointEarned
	case TypePolls:
		return TypePolls
	case TypeBroadcasterSettingsUpdate:
		return TypeBroadcasterSettingsUpdate
	}

	return TypeInvalid
}
