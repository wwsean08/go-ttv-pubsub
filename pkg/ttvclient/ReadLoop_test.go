package ttvclient

import (
	"encoding/json"
	"github.com/stretchr/testify/require"
	"testing"
)

func TestClient_handleTopics(t *testing.T) {
	// an empty message (or specifically an empty topic) causes it to return false
	client := Client{}
	shouldBeFalse := client.handleTopics(IncomingMessage{})
	require.False(t, shouldBeFalse)

	// Setup everything for other tests
	bitsReceived := false
	bitsBadgeReceived := false
	subscriptionReceived := false
	commerceReceived := false
	whisperReceived := false
	moderationReceived := false
	channelPointsRedemptionReceived := false
	videoPlaybackReceived := false
	channelPointsEarnedReceived := false
	pollReceived := false
	broadcastSettingsUpdateReceived := false

	bitsHandler := func(msg BitsMsg) {
		bitsReceived = true
	}

	bitsBadgeHandler := func(msg BitsBadgeMsg) {
		bitsBadgeReceived = true
	}

	subscriptionHandler := func(msg SubscriptionMsg) {
		subscriptionReceived = true
	}

	commerceHandler := func(msg CommerceMsg) {
		commerceReceived = true
	}

	whisperHandler := func(msg WhisperMsg) {
		whisperReceived = true
	}

	moderationHandler := func(msg ModerationActionMsg) {
		moderationReceived = true
	}
	channelPointsRedeemedHandler := func(msg ChannelPointRedemptionMsg) {
		channelPointsRedemptionReceived = true
	}

	videoPlaybackHandler := func(msg VideoPlaybackMsg) {
		videoPlaybackReceived = true
	}

	channelPointsEarnedHandler := func(msg ChannelPointEarnedMsg) {
		channelPointsEarnedReceived = true
	}

	pollHandler := func(msg PollMsg) {
		pollReceived = true
	}

	broadcastSettingsUpdateHandler := func(msg BroadcastSettingsUpdateMsg) {
		broadcastSettingsUpdateReceived = true
	}

	client.SetBitsHandler(bitsHandler)
	client.SetBitsBadgeHandler(bitsBadgeHandler)
	client.SetSubscriptionsHandler(subscriptionHandler)
	client.SetCommerceHandler(commerceHandler)
	client.SetWhisperHandler(whisperHandler)
	client.SetModerationHandler(moderationHandler)
	client.SetChannelPointRedemptionHandler(channelPointsRedeemedHandler)
	client.SetVideoPlaybackByIDHandler(videoPlaybackHandler)
	client.SetChannelPointEarnedHandler(channelPointsEarnedHandler)
	client.SetPollHandler(pollHandler)
	client.SetBroadcastSettingsUpdateHandler(broadcastSettingsUpdateHandler)

	type TestCases struct {
		message                         string
		bitsExpected                    bool
		bitsBadgeExpected               bool
		subscriptionExpected            bool
		commerceExpected                bool
		whisperExpected                 bool
		moderationExpected              bool
		channelPointRedemptionExpected  bool
		videoPlaybackExpected           bool
		channelPointEarnedExpected      bool
		pollExpected                    bool
		broadcastSettingsUpdateExpected bool
		expectedReturnCode              bool
		errorMessage                    string
	}

	table := []TestCases{
		// BEGIN bits tests
		{
			message:                         "{\"type\":\"MESSAGE\",\"data\":{\"topic\":\"channel-bits-events-v2.46024993\",\"message\":\"{\\\"data\\\":{\\\"user_name\\\":\\\"jwp\\\",\\\"channel_name\\\":\\\"bontakun\\\",\\\"user_id\\\":\\\"95546976\\\",\\\"channel_id\\\":\\\"46024993\\\",\\\"time\\\":\\\"2017-02-09T13:23:58.168Z\\\",\\\"chat_message\\\":\\\"cheer10000 New badge hype!\\\",\\\"bits_used\\\":10000,\\\"total_bits_used\\\":25000,\\\"context\\\":\\\"cheer\\\",\\\"badge_entitlement\\\":{\\\"new_version\\\":25000,\\\"previous_version\\\":10000}},\\\"version\\\":\\\"1.0\\\",\\\"message_type\\\":\\\"bits_event\\\",\\\"message_id\\\":\\\"8145728a4-35f0-4cf7-9dc0-f2ef24de1eb6\\\",\\\"is_anonymous\\\":true}\"}}",
			bitsExpected:                    true,
			bitsBadgeExpected:               false,
			subscriptionExpected:            false,
			commerceExpected:                false,
			whisperExpected:                 false,
			moderationExpected:              false,
			channelPointRedemptionExpected:  false,
			videoPlaybackExpected:           false,
			channelPointEarnedExpected:      false,
			pollExpected:                    false,
			broadcastSettingsUpdateExpected: false,
			expectedReturnCode:              true,
			errorMessage:                    "Error parsing or handling channel bits message",
		},
		{
			message:                         "{\"type\":\"MESSAGE\",\"data\":{\"topic\":\"channel-bits-events-v2.46024993\",\"message\":\"{\\\"data\\\":{\\\"user_name\\\":\\\"jwp\\\",\\\"channel_name\\\":\\\"bontakun\\\",\\\"user_id\\\":95546976,\\\"channel_id\\\":\\\"46024993\\\",\\\"time\\\":\\\"2017-02-09T13:23:58.168Z\\\",\\\"chat_message\\\":\\\"cheer10000 New badge hype!\\\",\\\"bits_used\\\":10000,\\\"total_bits_used\\\":25000,\\\"context\\\":\\\"cheer\\\",\\\"badge_entitlement\\\":{\\\"new_version\\\":25000,\\\"previous_version\\\":10000}},\\\"version\\\":\\\"1.0\\\",\\\"message_type\\\":\\\"bits_event\\\",\\\"message_id\\\":\\\"8145728a4-35f0-4cf7-9dc0-f2ef24de1eb6\\\",\\\"is_anonymous\\\":true}\"}}",
			bitsExpected:                    false,
			bitsBadgeExpected:               false,
			subscriptionExpected:            false,
			commerceExpected:                false,
			whisperExpected:                 false,
			moderationExpected:              false,
			channelPointRedemptionExpected:  false,
			videoPlaybackExpected:           false,
			channelPointEarnedExpected:      false,
			pollExpected:                    false,
			broadcastSettingsUpdateExpected: false,
			expectedReturnCode:              false,
			errorMessage:                    "Invalid bits message handled somehow",
		},
		// END bits tests
		// BEGIN bits badge tests
		{
			message:                         "{ \"type\":\"MESSAGE\",\"data\":{\"topic\":\"channel-bits-badge-unlocks.401394874\",\"message\":\" { \\\"user_id\\\":\\\"232889822\\\",\\\"user_name\\\":\\\"willowolf\\\",\\\"channel_id\\\":\\\"401394874\\\",\\\"channel_name\\\":\\\"fun_test12345\\\",\\\"badge_tier\\\":1000,\\\"chat_message\\\":\\\"this should be received by the public pubsub listener\\\",\\\"time\\\":\\\"2020-12-06T00:01:43.71253159Z\\\"}\" } }",
			bitsExpected:                    false,
			bitsBadgeExpected:               true,
			subscriptionExpected:            false,
			commerceExpected:                false,
			whisperExpected:                 false,
			moderationExpected:              false,
			channelPointRedemptionExpected:  false,
			videoPlaybackExpected:           false,
			channelPointEarnedExpected:      false,
			pollExpected:                    false,
			broadcastSettingsUpdateExpected: false,
			expectedReturnCode:              true,
			errorMessage:                    "Error parsing or handling channel bits badge unlock message",
		},
		{
			message:                         "{ \"type\":\"MESSAGE\",\"data\":{\"topic\":\"channel-bits-badge-unlocks.401394874\",\"message\":\" { \\\"user_id\\\":232889822,\\\"user_name\\\":\\\"willowolf\\\",\\\"channel_id\\\":\\\"401394874\\\",\\\"channel_name\\\":\\\"fun_test12345\\\",\\\"badge_tier\\\":1000,\\\"chat_message\\\":\\\"this should be received by the public pubsub listener\\\",\\\"time\\\":\\\"2020-12-06T00:01:43.71253159Z\\\"}\" } }",
			bitsExpected:                    false,
			bitsBadgeExpected:               false,
			subscriptionExpected:            false,
			commerceExpected:                false,
			whisperExpected:                 false,
			moderationExpected:              false,
			channelPointRedemptionExpected:  false,
			videoPlaybackExpected:           false,
			channelPointEarnedExpected:      false,
			pollExpected:                    false,
			broadcastSettingsUpdateExpected: false,
			expectedReturnCode:              false,
			errorMessage:                    "Invalid bits badge unlock message handled somehow",
		},
		// END bits badge tests
		// BEGIN subscription tests
		{
			message:                         "{\"type\": \"MESSAGE\", \"data\": {\"topic\": \"channel-subscribe-events-v1.44322889\", \"message\": \"{\\\"user_name\\\": \\\"dallas\\\", \\\"display_name\\\": \\\"dallas\\\", \\\"channel_name\\\": \\\"twitch\\\", \\\"user_id\\\": \\\"44322889\\\", \\\"channel_id\\\": \\\"12826\\\",\\\"time\\\": \\\"2015-12-19T16:39:57-08:00\\\",\\\"sub_plan\\\": \\\"Prime\\\",\\\"sub_plan_name\\\": \\\"Channel Subscription (mr_woodchuck)\\\",\\\"cumulative-months\\\": 9,\\\"streak-months\\\": 3,\\\"context\\\": \\\"sub\\\",\\\"sub_message\\\": {\\\"message\\\": \\\"A Twitch baby is born! KappaHD\\\", \\\"emotes\\\": [{\\\"start\\\": 23, \\\"end\\\": 7, \\\"id\\\": 2867}]}}\"}}",
			bitsExpected:                    false,
			bitsBadgeExpected:               false,
			subscriptionExpected:            true,
			commerceExpected:                false,
			whisperExpected:                 false,
			moderationExpected:              false,
			channelPointRedemptionExpected:  false,
			videoPlaybackExpected:           false,
			channelPointEarnedExpected:      false,
			pollExpected:                    false,
			broadcastSettingsUpdateExpected: false,
			expectedReturnCode:              true,
			errorMessage:                    "Error parsing or handling subscription message from user sub or resub",
		},
		{
			message:                         "{\"type\": \"MESSAGE\", \"data\": {\"topic\": \"channel-subscribe-events-v1.44322889\", \"message\": \"{\\\"user_name\\\": \\\"dallas\\\", \\\"display_name\\\": \\\"dallas\\\", \\\"channel_name\\\": \\\"twitch\\\", \\\"user_id\\\": \\\"44322889\\\", \\\"channel_id\\\": 12826,\\\"time\\\": \\\"2015-12-19T16:39:57-08:00\\\",\\\"sub_plan\\\": \\\"Prime\\\",\\\"sub_plan_name\\\": \\\"Channel Subscription (mr_woodchuck)\\\",\\\"cumulative-months\\\": 9,\\\"streak-months\\\": 3,\\\"context\\\": \\\"sub\\\",\\\"sub_message\\\": {\\\"message\\\": \\\"A Twitch baby is born! KappaHD\\\", \\\"emotes\\\": [{\\\"start\\\": 23, \\\"end\\\": 7, \\\"id\\\": 2867}]}}\"}}",
			bitsExpected:                    false,
			bitsBadgeExpected:               false,
			subscriptionExpected:            false,
			commerceExpected:                false,
			whisperExpected:                 false,
			moderationExpected:              false,
			channelPointRedemptionExpected:  false,
			videoPlaybackExpected:           false,
			channelPointEarnedExpected:      false,
			pollExpected:                    false,
			broadcastSettingsUpdateExpected: false,
			expectedReturnCode:              false,
			errorMessage:                    "Invalid subscription message from user sub or resub handled somehow",
		},
		{
			message:                         "{\"type\": \"MESSAGE\", \"data\": {\"topic\": \"channel-subscribe-events-v1.44322889\", \"message\": \"{\\\"user_name\\\": \\\"dallas\\\", \\\"display_name\\\": \\\"dallas\\\", \\\"channel_name\\\": \\\"twitch\\\", \\\"user_id\\\": \\\"44322889\\\", \\\"channel_id\\\": \\\"12826\\\", \\\"time\\\": \\\"2015-12-19T16:39:57-08:00\\\", \\\"sub_plan\\\": \\\"1000\\\", \\\"sub_plan_name\\\": \\\"Channel Subscription (mr_woodchuck)\\\", \\\"months\\\": 9, \\\"context\\\": \\\"subgift\\\", \\\"sub_message\\\": {\\\"message\\\": \\\"\\\", \\\"emotes\\\": null }, \\\"recipient_id\\\": \\\"13405587\\\", \\\"recipient_user_name\\\": \\\"tww2\\\", \\\"recipient_display_name\\\": \\\"TWW2\\\"}\"}}",
			bitsExpected:                    false,
			bitsBadgeExpected:               false,
			subscriptionExpected:            true,
			commerceExpected:                false,
			whisperExpected:                 false,
			moderationExpected:              false,
			channelPointRedemptionExpected:  false,
			videoPlaybackExpected:           false,
			channelPointEarnedExpected:      false,
			pollExpected:                    false,
			broadcastSettingsUpdateExpected: false,
			expectedReturnCode:              true,
			errorMessage:                    "Error parsing or handling subscription message from gifted subscription",
		},
		{
			message:                         "{\"type\": \"MESSAGE\", \"data\": {\"topic\": \"channel-subscribe-events-v1.44322889\", \"message\": \"{\\\"user_name\\\": \\\"dallas\\\", \\\"display_name\\\": \\\"dallas\\\", \\\"channel_name\\\": \\\"twitch\\\", \\\"user_id\\\": 44322889, \\\"channel_id\\\": \\\"12826\\\", \\\"time\\\": \\\"2015-12-19T16:39:57-08:00\\\", \\\"sub_plan\\\": \\\"1000\\\", \\\"sub_plan_name\\\": \\\"Channel Subscription (mr_woodchuck)\\\", \\\"months\\\": 9, \\\"context\\\": \\\"subgift\\\", \\\"sub_message\\\": {\\\"message\\\": \\\"\\\", \\\"emotes\\\": null }, \\\"recipient_id\\\": \\\"13405587\\\", \\\"recipient_user_name\\\": \\\"tww2\\\", \\\"recipient_display_name\\\": \\\"TWW2\\\"}\"}}",
			bitsExpected:                    false,
			bitsBadgeExpected:               false,
			subscriptionExpected:            false,
			commerceExpected:                false,
			whisperExpected:                 false,
			moderationExpected:              false,
			channelPointRedemptionExpected:  false,
			videoPlaybackExpected:           false,
			channelPointEarnedExpected:      false,
			pollExpected:                    false,
			broadcastSettingsUpdateExpected: false,
			expectedReturnCode:              false,
			errorMessage:                    "Invalid subscription message from gifted subscription handled somehow",
		},
		{
			message:                         "{\"type\": \"MESSAGE\", \"data\": {\"topic\": \"channel-subscribe-events-v1.44322889\", \"message\": \"{\\\"channel_name\\\": \\\"twitch\\\", \\\"channel_id\\\": \\\"12826\\\", \\\"time\\\": \\\"2015-12-19T16:39:57-08:00\\\", \\\"sub_plan\\\": \\\"1000\\\", \\\"sub_plan_name\\\": \\\"Channel Subscription (mr_woodchuck)\\\", \\\"months\\\": 9, \\\"context\\\": \\\"anonsubgift\\\", \\\"sub_message\\\": {\\\"message\\\": \\\"\\\", \\\"emotes\\\": null }, \\\"recipient_id\\\": \\\"13405587\\\", \\\"recipient_user_name\\\": \\\"tww2\\\", \\\"recipient_display_name\\\": \\\"TWW2\\\"}\"}}",
			bitsExpected:                    false,
			bitsBadgeExpected:               false,
			subscriptionExpected:            true,
			commerceExpected:                false,
			whisperExpected:                 false,
			moderationExpected:              false,
			channelPointRedemptionExpected:  false,
			videoPlaybackExpected:           false,
			channelPointEarnedExpected:      false,
			pollExpected:                    false,
			broadcastSettingsUpdateExpected: false,
			expectedReturnCode:              true,
			errorMessage:                    "Error parsing or handling subscription message from anonymous gifted subscription",
		},
		{
			message:                         "{\"type\": \"MESSAGE\", \"data\": {\"topic\": \"channel-subscribe-events-v1.44322889\", \"message\": \"{\\\"channel_name\\\": \\\"twitch\\\", \\\"channel_id\\\": 12826, \\\"time\\\": \\\"2015-12-19T16:39:57-08:00\\\", \\\"sub_plan\\\": \\\"1000\\\", \\\"sub_plan_name\\\": \\\"Channel Subscription (mr_woodchuck)\\\", \\\"months\\\": 9, \\\"context\\\": \\\"anonsubgift\\\", \\\"sub_message\\\": {\\\"message\\\": \\\"\\\", \\\"emotes\\\": null }, \\\"recipient_id\\\": \\\"13405587\\\", \\\"recipient_user_name\\\": \\\"tww2\\\", \\\"recipient_display_name\\\": \\\"TWW2\\\"}\"}}",
			bitsExpected:                    false,
			bitsBadgeExpected:               false,
			subscriptionExpected:            false,
			commerceExpected:                false,
			whisperExpected:                 false,
			moderationExpected:              false,
			channelPointRedemptionExpected:  false,
			videoPlaybackExpected:           false,
			channelPointEarnedExpected:      false,
			pollExpected:                    false,
			broadcastSettingsUpdateExpected: false,
			expectedReturnCode:              false,
			errorMessage:                    "Invalid subscription message from anonymous gifted subscription handled somehow",
		},
		{
			message:                         "{\"type\": \"MESSAGE\", \"data\": {\"topic\": \"channel-subscribe-events-v1.44322889\", \"message\": \"{\\\"user_name\\\": \\\"tww2\\\", \\\"display_name\\\": \\\"TWW2\\\", \\\"channel_name\\\": \\\"mr_woodchuck\\\", \\\"user_id\\\": \\\"13405587\\\", \\\"channel_id\\\": \\\"89614178\\\", \\\"time\\\": \\\"2015-12-19T16:39:57-08:00\\\", \\\"sub_plan\\\": \\\"1000\\\", \\\"sub_plan_name\\\": \\\"Channel Subscription (mr_woodchuck)\\\", \\\"months\\\": 9, \\\"context\\\": \\\"subgift\\\", \\\"is_gift\\\": true, \\\"sub_message\\\": {\\\"message\\\": \\\"\\\", \\\"emotes\\\": null}, \\\"recipient_id\\\": \\\"19571752\\\", \\\"recipient_user_name\\\": \\\"forstycup\\\", \\\"recipient_display_name\\\": \\\"forstycup\\\", \\\"multi_month_duration\\\": 6}\"}}",
			bitsExpected:                    false,
			bitsBadgeExpected:               false,
			subscriptionExpected:            true,
			commerceExpected:                false,
			whisperExpected:                 false,
			moderationExpected:              false,
			channelPointRedemptionExpected:  false,
			videoPlaybackExpected:           false,
			channelPointEarnedExpected:      false,
			pollExpected:                    false,
			broadcastSettingsUpdateExpected: false,
			expectedReturnCode:              true,
			errorMessage:                    "Error parsing or handling subscription message with multi-month gift",
		},
		// END subscription tests
		// BEGIN commerce tests
		{
			message:                         "{\"type\": \"MESSAGE\", \"data\": {\"topic\": \"channel-commerce-events-v1.44322889\", \"message\": \"{\\\"user_name\\\": \\\"dallas\\\", \\\"display_name\\\": \\\"dallas\\\", \\\"channel_name\\\": \\\"twitch\\\", \\\"user_id\\\": \\\"44322889\\\", \\\"channel_id\\\": \\\"12826\\\", \\\"time\\\": \\\"2015-12-19T16:39:57-08:00\\\", \\\"item_image_url\\\": \\\"https://...\\\", \\\"item_description\\\": \\\"This is a friendly description!\\\", \\\"supports_channel\\\": true, \\\"purchase_message\\\": {\\\"message\\\": \\\"A Twitch game is born! Kappa\\\", \\\"emotes\\\": [{\\\"start\\\": 23, \\\"end\\\": 7, \\\"id\\\": 2867}]}}\"}}",
			bitsExpected:                    false,
			bitsBadgeExpected:               false,
			subscriptionExpected:            false,
			commerceExpected:                true,
			whisperExpected:                 false,
			moderationExpected:              false,
			channelPointRedemptionExpected:  false,
			videoPlaybackExpected:           false,
			channelPointEarnedExpected:      false,
			pollExpected:                    false,
			broadcastSettingsUpdateExpected: false,
			expectedReturnCode:              true,
			errorMessage:                    "Error parsing or handling commerce message",
		},
		{
			message:                         "{\"type\": \"MESSAGE\", \"data\": {\"topic\": \"channel-commerce-events-v1.44322889\", \"message\": \"{\\\"user_name\\\": \\\"dallas\\\", \\\"display_name\\\": \\\"dallas\\\", \\\"channel_name\\\": \\\"twitch\\\", \\\"user_id\\\": 44322889, \\\"channel_id\\\": \\\"12826\\\", \\\"time\\\": \\\"2015-12-19T16:39:57-08:00\\\", \\\"item_image_url\\\": \\\"https://...\\\", \\\"item_description\\\": \\\"This is a friendly description!\\\", \\\"supports_channel\\\": true, \\\"purchase_message\\\": {\\\"message\\\": \\\"A Twitch game is born! Kappa\\\", \\\"emotes\\\": [{\\\"start\\\": 23, \\\"end\\\": 7, \\\"id\\\": 2867}]}}\"}}",
			bitsExpected:                    false,
			bitsBadgeExpected:               false,
			subscriptionExpected:            false,
			commerceExpected:                false,
			whisperExpected:                 false,
			moderationExpected:              false,
			channelPointRedemptionExpected:  false,
			videoPlaybackExpected:           false,
			channelPointEarnedExpected:      false,
			pollExpected:                    false,
			broadcastSettingsUpdateExpected: false,
			expectedReturnCode:              false,
			errorMessage:                    "Invalid commerce message handled somehow",
		},
		// END commerce tests
		// BEGIN whisper tests
		{
			message:                         "{\"type\": \"MESSAGE\", \"data\": {\"topic\": \"whispers.44322889\", \"message\": \"{\\\"type\\\":\\\"whisper_received\\\", \\\"data\\\":{\\\"id\\\":\\\"41\\\"}, \\\"thread_id\\\":\\\"129454141_44322889\\\", \\\"body\\\":\\\"hello\\\", \\\"sent_ts\\\":1479160009, \\\"from_id\\\":39141793, \\\"tags\\\":{\\\"login\\\":\\\"dallas\\\", \\\"display_name\\\":\\\"dallas\\\", \\\"color\\\":\\\"#8A2BE2\\\", \\\"emotes\\\":[], \\\"badges\\\":[{\\\"id\\\":\\\"staff\\\",\\\"version\\\":\\\"1\\\"}]}, \\\"recipient\\\":{\\\"id\\\":129454141, \\\"username\\\":\\\"dallasnchains\\\", \\\"display_name\\\":\\\"dallasnchains\\\", \\\"color\\\":\\\"\\\", \\\"badges\\\":[]}, \\\"nonce\\\":\\\"6GVBTfBXNj7d71BULYKjpiKapegDI1\\\"}\", \"data_object\": {\"id\": 41, \"thread_id\": \"129454141_44322889\", \"body\": \"hello\", \"sent_ts\": 1479160009, \"from_id\": 44322889, \"tags\": {\"login\": \"dallas\", \"display_name\": \"dallas\", \"color\": \"#8A2BE2\", \"emotes\": [], \"badges\": [{\"id\": \"staff\", \"version\": \"1\"}]}, \"recipient\": {\"id\": 129454141, \"username\": \"dallasnchains\", \"display_name\": \"dallasnchains\", \"color\": \"\", \"badges\": []}, \"nonce\": \"6GVBTfBXNj7d71BULYKjpiKapegDI1\"}}}",
			bitsExpected:                    false,
			bitsBadgeExpected:               false,
			subscriptionExpected:            false,
			commerceExpected:                false,
			whisperExpected:                 true,
			moderationExpected:              false,
			channelPointRedemptionExpected:  false,
			videoPlaybackExpected:           false,
			channelPointEarnedExpected:      false,
			pollExpected:                    false,
			broadcastSettingsUpdateExpected: false,
			expectedReturnCode:              true,
			errorMessage:                    "Error parsing or handling whisper message",
		},
		{
			message:                         "{\"type\": \"MESSAGE\", \"data\": {\"topic\": \"whispers.44322889\", \"message\": \"{\\\"type\\\":\\\"whisper_received\\\", \\\"data\\\":{\\\"id\\\":41}, \\\"thread_id\\\":\\\"129454141_44322889\\\", \\\"body\\\":\\\"hello\\\", \\\"sent_ts\\\":1479160009, \\\"from_id\\\":39141793, \\\"tags\\\":{\\\"login\\\":\\\"dallas\\\", \\\"display_name\\\":\\\"dallas\\\", \\\"color\\\":\\\"#8A2BE2\\\", \\\"emotes\\\":[], \\\"badges\\\":[{\\\"id\\\":\\\"staff\\\",\\\"version\\\":\\\"1\\\"}]}, \\\"recipient\\\":{\\\"id\\\":129454141, \\\"username\\\":\\\"dallasnchains\\\", \\\"display_name\\\":\\\"dallasnchains\\\", \\\"color\\\":\\\"\\\", \\\"badges\\\":[]}, \\\"nonce\\\":\\\"6GVBTfBXNj7d71BULYKjpiKapegDI1\\\"}\", \"data_object\": {\"id\": 41, \"thread_id\": \"129454141_44322889\", \"body\": \"hello\", \"sent_ts\": 1479160009, \"from_id\": 44322889, \"tags\": {\"login\": \"dallas\", \"display_name\": \"dallas\", \"color\": \"#8A2BE2\", \"emotes\": [], \"badges\": [{\"id\": \"staff\", \"version\": \"1\"}]}, \"recipient\": {\"id\": 129454141, \"username\": \"dallasnchains\", \"display_name\": \"dallasnchains\", \"color\": \"\", \"badges\": []}, \"nonce\": \"6GVBTfBXNj7d71BULYKjpiKapegDI1\"}}}",
			bitsExpected:                    false,
			bitsBadgeExpected:               false,
			subscriptionExpected:            false,
			commerceExpected:                false,
			whisperExpected:                 false,
			moderationExpected:              false,
			channelPointRedemptionExpected:  false,
			videoPlaybackExpected:           false,
			channelPointEarnedExpected:      false,
			pollExpected:                    false,
			broadcastSettingsUpdateExpected: false,
			expectedReturnCode:              false,
			errorMessage:                    "Invalid whisper message got handled somehow",
		},
		// END whisper tests
		// BEGIN mod action tests
		{
			message:                         "{\"type\": \"MESSAGE\", \"data\": {\"topic\": \"chat_moderator_actions.47073625.47073625\", \"message\": \"{\\\"data\\\":{\\\"type\\\":\\\"chat_login_moderation\\\",\\\"moderation_action\\\":\\\"untimeout\\\",\\\"args\\\":[\\\"anthonyonstickz\\\"],\\\"created_by\\\":\\\"wwsean08\\\",\\\"created_by_user_id\\\":\\\"47073625\\\",\\\"msg_id\\\":\\\"\\\",\\\"target_user_id\\\":\\\"404287440\\\",\\\"target_user_login\\\":\\\"\\\",\\\"from_automod\\\":false}}\"}}\n",
			bitsExpected:                    false,
			bitsBadgeExpected:               false,
			subscriptionExpected:            false,
			commerceExpected:                false,
			whisperExpected:                 false,
			moderationExpected:              true,
			channelPointRedemptionExpected:  false,
			videoPlaybackExpected:           false,
			channelPointEarnedExpected:      false,
			pollExpected:                    false,
			broadcastSettingsUpdateExpected: false,
			expectedReturnCode:              true,
			errorMessage:                    "Error parsing or handling mod action message",
		},
		{
			message:                         "{\"type\": \"MESSAGE\", \"data\": {\"topic\": \"chat_moderator_actions.47073625.47073625\", \"message\": \"{\\\"data\\\":{\\\"type\\\":\\\"chat_login_moderation\\\",\\\"moderation_action\\\":\\\"untimeout\\\",\\\"args\\\":[\\\"anthonyonstickz\\\"],\\\"created_by\\\":\\\"wwsean08\\\",\\\"created_by_user_id\\\":47073625,\\\"msg_id\\\":\\\"\\\",\\\"target_user_id\\\":\\\"404287440\\\",\\\"target_user_login\\\":\\\"\\\",\\\"from_automod\\\":false}}\"}}\n",
			bitsExpected:                    false,
			bitsBadgeExpected:               false,
			subscriptionExpected:            false,
			commerceExpected:                false,
			whisperExpected:                 false,
			moderationExpected:              false,
			channelPointRedemptionExpected:  false,
			videoPlaybackExpected:           false,
			channelPointEarnedExpected:      false,
			pollExpected:                    false,
			broadcastSettingsUpdateExpected: false,
			expectedReturnCode:              false,
			errorMessage:                    "Invalid moderator action message got handled somehow",
		},
		// END mod action tests
		// BEGIN channel points redeemed tests
		{
			message:                         "{\"type\": \"MESSAGE\", \"data\": {\"message\": \"{\\\"type\\\":\\\"reward-redeemed\\\",\\\"data\\\":{\\\"timestamp\\\":\\\"2020-05-15T23:04:10.398006664Z\\\",\\\"redemption\\\":{\\\"id\\\":\\\"264467c5-549f-4774-afa7-31fd975e08ad\\\",\\\"user\\\":{\\\"id\\\":\\\"47073625\\\",\\\"login\\\":\\\"wwsean08\\\",\\\"display_name\\\":\\\"wwsean08\\\"},\\\"channel_id\\\":\\\"47073625\\\",\\\"redeemed_at\\\":\\\"2020-05-15T23:04:10.373830706Z\\\",\\\"reward\\\":{\\\"id\\\":\\\"c33057d1-e922-482d-922b-49645ac5c222\\\",\\\"channel_id\\\":\\\"47073625\\\",\\\"title\\\":\\\"Hydrate!\\\",\\\"prompt\\\":\\\"Make me take a sip of water\\\",\\\"cost\\\":100,\\\"is_user_input_required\\\":false,\\\"is_sub_only\\\":false,\\\"image\\\":null,\\\"default_image\\\":{\\\"url_1x\\\":\\\"https://static-cdn.jtvnw.net/custom-reward-images/tree-1.png\\\",\\\"url_2x\\\":\\\"https://static-cdn.jtvnw.net/custom-reward-images/tree-2.png\\\",\\\"url_4x\\\":\\\"https://static-cdn.jtvnw.net/custom-reward-images/tree-4.png\\\"},\\\"background_color\\\":\\\"#00E5CB\\\",\\\"is_enabled\\\":true,\\\"is_paused\\\":false,\\\"is_in_stock\\\":true,\\\"max_per_stream\\\":{\\\"is_enabled\\\":false,\\\"max_per_stream\\\":1},\\\"should_redemptions_skip_request_queue\\\":false,\\\"template_id\\\":\\\"template:41d5eae8-4deb-4541-b681-ebdcb3125c0f\\\",\\\"updated_for_indicator_at\\\":\\\"2020-05-01T00:32:38.049474943Z\\\"},\\\"status\\\":\\\"UNFULFILLED\\\"}}}\", \"topic\": \"channel-points-channel-v1.47073625\"}, \"nonce\": \"\", \"error\": \"\"}\n",
			bitsExpected:                    false,
			bitsBadgeExpected:               false,
			subscriptionExpected:            false,
			commerceExpected:                false,
			whisperExpected:                 false,
			moderationExpected:              false,
			channelPointRedemptionExpected:  true,
			videoPlaybackExpected:           false,
			channelPointEarnedExpected:      false,
			pollExpected:                    false,
			broadcastSettingsUpdateExpected: false,
			expectedReturnCode:              true,
			errorMessage:                    "Error parsing or handling channel point redemption message",
		},
		// END channel point redemption tests
		// BEGIN video playback by id tests
		{
			message:                         "{\"type\": \"MESSAGE\", \"data\": {\"topic\": \"video-playback-by-id.252897252\", \"message\": \"{\\\"type\\\":\\\"viewcount\\\",\\\"server_time\\\":1590959468.335576,\\\"viewers\\\":58}\"}}\n",
			bitsExpected:                    false,
			bitsBadgeExpected:               false,
			subscriptionExpected:            false,
			commerceExpected:                false,
			whisperExpected:                 false,
			moderationExpected:              false,
			channelPointRedemptionExpected:  false,
			videoPlaybackExpected:           true,
			channelPointEarnedExpected:      false,
			pollExpected:                    false,
			broadcastSettingsUpdateExpected: false,
			expectedReturnCode:              true,
			errorMessage:                    "Error parsing or handling channel point redemption message",
		},
		{
			message:                         "{\"type\": \"MESSAGE\", \"data\": {\"topic\": \"video-playback-by-id.252897252\", \"message\": \"{\\\"type\\\":\\\"viewcount\\\",\\\"server_time\\\":\\\"1590959468.335576\\\",\\\"viewers\\\":58}\"}}\n",
			bitsExpected:                    false,
			bitsBadgeExpected:               false,
			subscriptionExpected:            false,
			commerceExpected:                false,
			whisperExpected:                 false,
			moderationExpected:              false,
			channelPointRedemptionExpected:  false,
			videoPlaybackExpected:           false,
			channelPointEarnedExpected:      false,
			pollExpected:                    false,
			broadcastSettingsUpdateExpected: false,
			expectedReturnCode:              false,
			errorMessage:                    "Invalid video playback by id message got handled somehow",
		},
		// END video playback by id tests
		// BEGIN channel point earned tests
		{
			message:                         "{\"type\": \"MESSAGE\", \"data\": {\"topic\": \"community-points-user-v1.47073625\", \"message\": \"{\\\"type\\\":\\\"claim-available\\\",\\\"data\\\":{\\\"timestamp\\\":\\\"2020-05-31T21:21:09.922073751Z\\\",\\\"claim\\\":{\\\"id\\\":\\\"820b4526-236f-4c8d-bd1c-7cef3ff25667\\\",\\\"user_id\\\":\\\"47073625\\\",\\\"channel_id\\\":\\\"252897252\\\",\\\"point_gain\\\":{\\\"user_id\\\":\\\"47073625\\\",\\\"channel_id\\\":\\\"252897252\\\",\\\"total_points\\\":100,\\\"baseline_points\\\":50,\\\"reason_code\\\":\\\"CLAIM\\\",\\\"multipliers\\\":[{\\\"reason_code\\\":\\\"SUB_T1\\\",\\\"factor\\\":1}]},\\\"created_at\\\":\\\"2020-05-31T21:21:06Z\\\"}}}\"}}",
			bitsExpected:                    false,
			bitsBadgeExpected:               false,
			subscriptionExpected:            false,
			commerceExpected:                false,
			whisperExpected:                 false,
			moderationExpected:              false,
			channelPointRedemptionExpected:  false,
			videoPlaybackExpected:           false,
			channelPointEarnedExpected:      true,
			pollExpected:                    false,
			broadcastSettingsUpdateExpected: false,
			expectedReturnCode:              true,
			errorMessage:                    "Error parsing or handling channel point earned message for claim-available",
		},
		{
			message:                         "{\"type\": \"MESSAGE\", \"data\": {\"topic\": \"community-points-user-v1.47073625\", \"message\": \"{\\\"type\\\":\\\"claim-available\\\",\\\"data\\\":{\\\"timestamp\\\":\\\"2020-05-31T21:21:09.922073751Z\\\",\\\"claim\\\":{\\\"id\\\":\\\"820b4526-236f-4c8d-bd1c-7cef3ff25667\\\",\\\"user_id\\\":\\\"47073625\\\",\\\"channel_id\\\":\\\"252897252\\\",\\\"point_gain\\\":{\\\"user_id\\\":\\\"47073625\\\",\\\"channel_id\\\":\\\"252897252\\\",\\\"total_points\\\":100,\\\"baseline_points\\\":50,\\\"reason_code\\\":\\\"CLAIM\\\",\\\"multipliers\\\":[{\\\"reason_code\\\":\\\"SUB_T1\\\",\\\"factor\\\":\\\"1\\\"}]},\\\"created_at\\\":\\\"2020-05-31T21:21:06Z\\\"}}}\"}}",
			bitsExpected:                    false,
			bitsBadgeExpected:               false,
			subscriptionExpected:            false,
			commerceExpected:                false,
			whisperExpected:                 false,
			moderationExpected:              false,
			channelPointRedemptionExpected:  false,
			videoPlaybackExpected:           false,
			channelPointEarnedExpected:      false,
			pollExpected:                    false,
			broadcastSettingsUpdateExpected: false,
			expectedReturnCode:              false,
			errorMessage:                    "Invalid channel point earned for claim-available message got handled somehow for claim-available",
		},
		{
			message:                         "{\"type\":\"MESSAGE\",\"data\":{\"topic\":\"community-points-user-v1.47073625\",\"message\":\"{\\\"type\\\":\\\"claim-claimed\\\",\\\"data\\\":{\\\"timestamp\\\":\\\"2020-05-31T21:36:22.898171319Z\\\",\\\"claim\\\":{\\\"id\\\":\\\"ef67b9f7-b66b-4e43-99e1-fbafc2778bc5\\\",\\\"user_id\\\":\\\"47073625\\\",\\\"channel_id\\\":\\\"252897252\\\",\\\"point_gain\\\":{\\\"user_id\\\":\\\"47073625\\\",\\\"channel_id\\\":\\\"252897252\\\",\\\"total_points\\\":100,\\\"baseline_points\\\":50,\\\"reason_code\\\":\\\"CLAIM\\\",\\\"multipliers\\\":[{\\\"reason_code\\\":\\\"SUB_T1\\\",\\\"factor\\\":1}]},\\\"created_at\\\":\\\"2020-05-31T21:36:06Z\\\"}}}\"}}",
			bitsExpected:                    false,
			bitsBadgeExpected:               false,
			subscriptionExpected:            false,
			commerceExpected:                false,
			whisperExpected:                 false,
			moderationExpected:              false,
			channelPointRedemptionExpected:  false,
			videoPlaybackExpected:           false,
			channelPointEarnedExpected:      true,
			pollExpected:                    false,
			broadcastSettingsUpdateExpected: false,
			expectedReturnCode:              true,
			errorMessage:                    "Error parsing or handling channel point earned message for claim-claimed",
		},
		{
			message:                         "{\"type\":\"MESSAGE\",\"data\":{\"topic\":\"community-points-user-v1.47073625\",\"message\":\"{\\\"type\\\":\\\"claim-claimed\\\",\\\"data\\\":{\\\"timestamp\\\":\\\"2020-05-31T21:36:22.898171319Z\\\",\\\"claim\\\":{\\\"id\\\":\\\"ef67b9f7-b66b-4e43-99e1-fbafc2778bc5\\\",\\\"user_id\\\":\\\"47073625\\\",\\\"channel_id\\\":\\\"252897252\\\",\\\"point_gain\\\":{\\\"user_id\\\":\\\"47073625\\\",\\\"channel_id\\\":\\\"252897252\\\",\\\"total_points\\\":100,\\\"baseline_points\\\":50,\\\"reason_code\\\":\\\"CLAIM\\\",\\\"multipliers\\\":[{\\\"reason_code\\\":\\\"SUB_T1\\\",\\\"factor\\\":\\\"1\\\"}]},\\\"created_at\\\":\\\"2020-05-31T21:36:06Z\\\"}}}\"}}",
			bitsExpected:                    false,
			bitsBadgeExpected:               false,
			subscriptionExpected:            false,
			commerceExpected:                false,
			whisperExpected:                 false,
			moderationExpected:              false,
			channelPointRedemptionExpected:  false,
			videoPlaybackExpected:           false,
			channelPointEarnedExpected:      false,
			pollExpected:                    false,
			broadcastSettingsUpdateExpected: false,
			expectedReturnCode:              false,
			errorMessage:                    "Invalid channel point earned message got handled somehow for claim-claimed",
		},
		{
			message:                         "{\"type\":\"MESSAGE\",\"data\":{\"topic\":\"community-points-user-v1.47073625\",\"message\":\"{\\\"type\\\":\\\"points-earned\\\",\\\"data\\\":{\\\"timestamp\\\":\\\"2020-05-31T21:21:09.915023535Z\\\",\\\"channel_id\\\":\\\"252897252\\\",\\\"point_gain\\\":{\\\"user_id\\\":\\\"47073625\\\",\\\"channel_id\\\":\\\"252897252\\\",\\\"total_points\\\":20,\\\"baseline_points\\\":10,\\\"reason_code\\\":\\\"WATCH\\\",\\\"multipliers\\\":[{\\\"reason_code\\\":\\\"SUB_T1\\\",\\\"factor\\\":1}]},\\\"balance\\\":{\\\"user_id\\\":\\\"47073625\\\",\\\"channel_id\\\":\\\"252897252\\\",\\\"balance\\\":68550}}}\"}}",
			bitsExpected:                    false,
			bitsBadgeExpected:               false,
			subscriptionExpected:            false,
			commerceExpected:                false,
			whisperExpected:                 false,
			moderationExpected:              false,
			channelPointRedemptionExpected:  false,
			videoPlaybackExpected:           false,
			channelPointEarnedExpected:      true,
			pollExpected:                    false,
			broadcastSettingsUpdateExpected: false,
			expectedReturnCode:              true,
			errorMessage:                    "Error parsing or handling channel point earned message for points-earned",
		},
		{
			message:                         "{\"type\":\"MESSAGE\",\"data\":{\"topic\":\"community-points-user-v1.47073625\",\"message\":\"{\\\"type\\\":\\\"points-earned\\\",\\\"data\\\":{\\\"timestamp\\\":\\\"2020-05-31T21:21:09.915023535Z\\\",\\\"channel_id\\\":252897252,\\\"point_gain\\\":{\\\"user_id\\\":\\\"47073625\\\",\\\"channel_id\\\":\\\"252897252\\\",\\\"total_points\\\":20,\\\"baseline_points\\\":10,\\\"reason_code\\\":\\\"WATCH\\\",\\\"multipliers\\\":[{\\\"reason_code\\\":\\\"SUB_T1\\\",\\\"factor\\\":1}]},\\\"balance\\\":{\\\"user_id\\\":\\\"47073625\\\",\\\"channel_id\\\":\\\"252897252\\\",\\\"balance\\\":68550}}}\"}}",
			bitsExpected:                    false,
			bitsBadgeExpected:               false,
			subscriptionExpected:            false,
			commerceExpected:                false,
			whisperExpected:                 false,
			moderationExpected:              false,
			channelPointRedemptionExpected:  false,
			videoPlaybackExpected:           false,
			channelPointEarnedExpected:      false,
			pollExpected:                    false,
			broadcastSettingsUpdateExpected: false,
			expectedReturnCode:              false,
			errorMessage:                    "Invalid channel point earned message got handled somehow for points-earned",
		},
		// END channel point earned tests
		// BEGIN poll tests
		{
			message:                         "{\"type\":\"MESSAGE\",\"data\":{\"topic\":\"polls.252897252\",\"message\":\"{\\\"type\\\":\\\"POLL_CREATE\\\",\\\"data\\\":{\\\"poll\\\":{\\\"poll_id\\\":\\\"4112662a-8148-4588-9a6e-6bac924fa180\\\",\\\"owned_by\\\":\\\"252897252\\\",\\\"created_by\\\":\\\"30477280\\\",\\\"title\\\":\\\"is anthony awesome\\\",\\\"started_at\\\":\\\"2020-05-31T21:13:12.087159836Z\\\",\\\"ended_at\\\":null,\\\"ended_by\\\":null,\\\"duration_seconds\\\":300,\\\"settings\\\":{\\\"multi_choice\\\":{\\\"is_enabled\\\":true},\\\"subscriber_only\\\":{\\\"is_enabled\\\":false},\\\"subscriber_multiplier\\\":{\\\"is_enabled\\\":false},\\\"bits_votes\\\":{\\\"is_enabled\\\":false,\\\"cost\\\":0},\\\"channel_points_votes\\\":{\\\"is_enabled\\\":false,\\\"cost\\\":0}},\\\"status\\\":\\\"ACTIVE\\\",\\\"choices\\\":[{\\\"choice_id\\\":\\\"19c8ac0e-3874-4398-9c9c-4ec7e21586a9\\\",\\\"title\\\":\\\"yes\\\",\\\"votes\\\":{\\\"total\\\":0,\\\"bits\\\":0,\\\"channel_points\\\":0,\\\"base\\\":0},\\\"tokens\\\":{\\\"bits\\\":0,\\\"channel_points\\\":0},\\\"total_voters\\\":0},{\\\"choice_id\\\":\\\"379c246d-6d7c-40c5-8a9f-47c0ba28f9f0\\\",\\\"title\\\":\\\"no\\\",\\\"votes\\\":{\\\"total\\\":0,\\\"bits\\\":0,\\\"channel_points\\\":0,\\\"base\\\":0},\\\"tokens\\\":{\\\"bits\\\":0,\\\"channel_points\\\":0},\\\"total_voters\\\":0}],\\\"votes\\\":{\\\"total\\\":0,\\\"bits\\\":0,\\\"channel_points\\\":0,\\\"base\\\":0},\\\"tokens\\\":{\\\"bits\\\":0,\\\"channel_points\\\":0},\\\"total_voters\\\":0,\\\"remaining_duration_milliseconds\\\":299985,\\\"top_contributor\\\":null,\\\"top_bits_contributor\\\":null,\\\"top_channel_points_contributor\\\":null}}}\"}}",
			bitsExpected:                    false,
			bitsBadgeExpected:               false,
			subscriptionExpected:            false,
			commerceExpected:                false,
			whisperExpected:                 false,
			moderationExpected:              false,
			channelPointRedemptionExpected:  false,
			videoPlaybackExpected:           false,
			channelPointEarnedExpected:      false,
			pollExpected:                    true,
			broadcastSettingsUpdateExpected: false,
			expectedReturnCode:              true,
			errorMessage:                    "Error parsing or handling poll message for poll created",
		},
		{
			message:                         "{\"type\":\"MESSAGE\",\"data\":{\"topic\":\"polls.252897252\",\"message\":\"{\\\"type\\\":\\\"POLL_CREATE\\\",\\\"data\\\":{\\\"poll\\\":{\\\"poll_id\\\":\\\"4112662a-8148-4588-9a6e-6bac924fa180\\\",\\\"owned_by\\\":\\\"252897252\\\",\\\"created_by\\\":\\\"30477280\\\",\\\"title\\\":\\\"is anthony awesome\\\",\\\"started_at\\\":\\\"2020-05-31T21:13:12.087159836Z\\\",\\\"ended_at\\\":null,\\\"ended_by\\\":null,\\\"duration_seconds\\\":300,\\\"settings\\\":{\\\"multi_choice\\\":{\\\"is_enabled\\\":true},\\\"subscriber_only\\\":{\\\"is_enabled\\\":false},\\\"subscriber_multiplier\\\":{\\\"is_enabled\\\":false},\\\"bits_votes\\\":{\\\"is_enabled\\\":false,\\\"cost\\\":0},\\\"channel_points_votes\\\":{\\\"is_enabled\\\":false,\\\"cost\\\":0}},\\\"status\\\":\\\"ACTIVE\\\",\\\"choices\\\":[{\\\"choice_id\\\":\\\"19c8ac0e-3874-4398-9c9c-4ec7e21586a9\\\",\\\"title\\\":\\\"yes\\\",\\\"votes\\\":{\\\"total\\\":0,\\\"bits\\\":0,\\\"channel_points\\\":0,\\\"base\\\":0},\\\"tokens\\\":{\\\"bits\\\":0,\\\"channel_points\\\":0},\\\"total_voters\\\":0},{\\\"choice_id\\\":\\\"379c246d-6d7c-40c5-8a9f-47c0ba28f9f0\\\",\\\"title\\\":\\\"no\\\",\\\"votes\\\":{\\\"total\\\":0,\\\"bits\\\":0,\\\"channel_points\\\":0,\\\"base\\\":0},\\\"tokens\\\":{\\\"bits\\\":0,\\\"channel_points\\\":0},\\\"total_voters\\\":0}],\\\"votes\\\":{\\\"total\\\":0,\\\"bits\\\":0,\\\"channel_points\\\":0,\\\"base\\\":0},\\\"tokens\\\":{\\\"bits\\\":0,\\\"channel_points\\\":0},\\\"total_voters\\\":0,\\\"remaining_duration_milliseconds\\\":\\\"299985\\\",\\\"top_contributor\\\":null,\\\"top_bits_contributor\\\":null,\\\"top_channel_points_contributor\\\":null}}}\"}}",
			bitsExpected:                    false,
			bitsBadgeExpected:               false,
			subscriptionExpected:            false,
			commerceExpected:                false,
			whisperExpected:                 false,
			moderationExpected:              false,
			channelPointRedemptionExpected:  false,
			videoPlaybackExpected:           false,
			channelPointEarnedExpected:      false,
			pollExpected:                    false,
			broadcastSettingsUpdateExpected: false,
			expectedReturnCode:              false,
			errorMessage:                    "Invalid poll message got handled somehow for poll created",
		},
		{
			message:                         "{\"type\":\"MESSAGE\",\"data\":{\"topic\":\"polls.252897252\",\"message\":\"{\\\"type\\\":\\\"POLL_UPDATE\\\",\\\"data\\\":{\\\"poll\\\":{\\\"poll_id\\\":\\\"4112662a-8148-4588-9a6e-6bac924fa180\\\",\\\"owned_by\\\":\\\"252897252\\\",\\\"created_by\\\":\\\"30477280\\\",\\\"title\\\":\\\"is anthony awesome\\\",\\\"started_at\\\":\\\"2020-05-31T21:13:12.087159836Z\\\",\\\"ended_at\\\":null,\\\"ended_by\\\":null,\\\"duration_seconds\\\":300,\\\"settings\\\":{\\\"multi_choice\\\":{\\\"is_enabled\\\":true},\\\"subscriber_only\\\":{\\\"is_enabled\\\":false},\\\"subscriber_multiplier\\\":{\\\"is_enabled\\\":false},\\\"bits_votes\\\":{\\\"is_enabled\\\":false,\\\"cost\\\":0},\\\"channel_points_votes\\\":{\\\"is_enabled\\\":false,\\\"cost\\\":0}},\\\"status\\\":\\\"ACTIVE\\\",\\\"choices\\\":[{\\\"choice_id\\\":\\\"19c8ac0e-3874-4398-9c9c-4ec7e21586a9\\\",\\\"title\\\":\\\"yes\\\",\\\"votes\\\":{\\\"total\\\":1,\\\"bits\\\":0,\\\"channel_points\\\":0,\\\"base\\\":1},\\\"tokens\\\":{\\\"bits\\\":0,\\\"channel_points\\\":0},\\\"total_voters\\\":0},{\\\"choice_id\\\":\\\"379c246d-6d7c-40c5-8a9f-47c0ba28f9f0\\\",\\\"title\\\":\\\"no\\\",\\\"votes\\\":{\\\"total\\\":0,\\\"bits\\\":0,\\\"channel_points\\\":0,\\\"base\\\":0},\\\"tokens\\\":{\\\"bits\\\":0,\\\"channel_points\\\":0},\\\"total_voters\\\":0}],\\\"votes\\\":{\\\"total\\\":1,\\\"bits\\\":0,\\\"channel_points\\\":0,\\\"base\\\":1},\\\"tokens\\\":{\\\"bits\\\":0,\\\"channel_points\\\":0},\\\"total_voters\\\":1,\\\"remaining_duration_milliseconds\\\":293553,\\\"top_contributor\\\":null,\\\"top_bits_contributor\\\":null,\\\"top_channel_points_contributor\\\":null}}}\"}}",
			bitsExpected:                    false,
			bitsBadgeExpected:               false,
			subscriptionExpected:            false,
			commerceExpected:                false,
			whisperExpected:                 false,
			moderationExpected:              false,
			channelPointRedemptionExpected:  false,
			videoPlaybackExpected:           false,
			channelPointEarnedExpected:      false,
			pollExpected:                    true,
			broadcastSettingsUpdateExpected: false,
			expectedReturnCode:              true,
			errorMessage:                    "Error parsing or handling poll message for poll updated",
		},
		{
			message:                         "{\"type\":\"MESSAGE\",\"data\":{\"topic\":\"polls.252897252\",\"message\":\"{\\\"type\\\":\\\"POLL_UPDATE\\\",\\\"data\\\":{\\\"poll\\\":{\\\"poll_id\\\":\\\"4112662a-8148-4588-9a6e-6bac924fa180\\\",\\\"owned_by\\\":\\\"252897252\\\",\\\"created_by\\\":\\\"30477280\\\",\\\"title\\\":\\\"is anthony awesome\\\",\\\"started_at\\\":\\\"2020-05-31T21:13:12.087159836Z\\\",\\\"ended_at\\\":null,\\\"ended_by\\\":null,\\\"duration_seconds\\\":300,\\\"settings\\\":{\\\"multi_choice\\\":{\\\"is_enabled\\\":true},\\\"subscriber_only\\\":{\\\"is_enabled\\\":false},\\\"subscriber_multiplier\\\":{\\\"is_enabled\\\":false},\\\"bits_votes\\\":{\\\"is_enabled\\\":false,\\\"cost\\\":0},\\\"channel_points_votes\\\":{\\\"is_enabled\\\":false,\\\"cost\\\":0}},\\\"status\\\":\\\"ACTIVE\\\",\\\"choices\\\":[{\\\"choice_id\\\":\\\"19c8ac0e-3874-4398-9c9c-4ec7e21586a9\\\",\\\"title\\\":\\\"yes\\\",\\\"votes\\\":{\\\"total\\\":1,\\\"bits\\\":0,\\\"channel_points\\\":0,\\\"base\\\":1},\\\"tokens\\\":{\\\"bits\\\":0,\\\"channel_points\\\":0},\\\"total_voters\\\":0},{\\\"choice_id\\\":\\\"379c246d-6d7c-40c5-8a9f-47c0ba28f9f0\\\",\\\"title\\\":\\\"no\\\",\\\"votes\\\":{\\\"total\\\":0,\\\"bits\\\":0,\\\"channel_points\\\":0,\\\"base\\\":0},\\\"tokens\\\":{\\\"bits\\\":0,\\\"channel_points\\\":0},\\\"total_voters\\\":0}],\\\"votes\\\":{\\\"total\\\":1,\\\"bits\\\":0,\\\"channel_points\\\":0,\\\"base\\\":1},\\\"tokens\\\":{\\\"bits\\\":0,\\\"channel_points\\\":0},\\\"total_voters\\\":1,\\\"remaining_duration_milliseconds\\\":\\\"293553\\\",\\\"top_contributor\\\":null,\\\"top_bits_contributor\\\":null,\\\"top_channel_points_contributor\\\":null}}}\"}}",
			bitsExpected:                    false,
			bitsBadgeExpected:               false,
			subscriptionExpected:            false,
			commerceExpected:                false,
			whisperExpected:                 false,
			moderationExpected:              false,
			channelPointRedemptionExpected:  false,
			videoPlaybackExpected:           false,
			channelPointEarnedExpected:      false,
			pollExpected:                    false,
			broadcastSettingsUpdateExpected: false,
			expectedReturnCode:              false,
			errorMessage:                    "Invalid poll message got handled somehow for poll updated",
		},
		{
			message:                         "{\"type\":\"MESSAGE\",\"data\":{\"topic\":\"polls.252897252\",\"message\":\"{\\\"type\\\":\\\"POLL_TERMINATE\\\",\\\"data\\\":{\\\"poll\\\":{\\\"poll_id\\\":\\\"4112662a-8148-4588-9a6e-6bac924fa180\\\",\\\"owned_by\\\":\\\"252897252\\\",\\\"created_by\\\":\\\"30477280\\\",\\\"title\\\":\\\"is anthony awesome\\\",\\\"started_at\\\":\\\"2020-05-31T21:13:12.087159836Z\\\",\\\"ended_at\\\":\\\"2020-05-31T21:14:02.382129552Z\\\",\\\"ended_by\\\":\\\"30477280\\\",\\\"duration_seconds\\\":300,\\\"settings\\\":{\\\"multi_choice\\\":{\\\"is_enabled\\\":true},\\\"subscriber_only\\\":{\\\"is_enabled\\\":false},\\\"subscriber_multiplier\\\":{\\\"is_enabled\\\":false},\\\"bits_votes\\\":{\\\"is_enabled\\\":false,\\\"cost\\\":0},\\\"channel_points_votes\\\":{\\\"is_enabled\\\":false,\\\"cost\\\":0}},\\\"status\\\":\\\"TERMINATED\\\",\\\"choices\\\":[{\\\"choice_id\\\":\\\"19c8ac0e-3874-4398-9c9c-4ec7e21586a9\\\",\\\"title\\\":\\\"yes\\\",\\\"votes\\\":{\\\"total\\\":5,\\\"bits\\\":0,\\\"channel_points\\\":0,\\\"base\\\":5},\\\"tokens\\\":{\\\"bits\\\":0,\\\"channel_points\\\":0},\\\"total_voters\\\":0},{\\\"choice_id\\\":\\\"379c246d-6d7c-40c5-8a9f-47c0ba28f9f0\\\",\\\"title\\\":\\\"no\\\",\\\"votes\\\":{\\\"total\\\":5,\\\"bits\\\":0,\\\"channel_points\\\":0,\\\"base\\\":5},\\\"tokens\\\":{\\\"bits\\\":0,\\\"channel_points\\\":0},\\\"total_voters\\\":0}],\\\"votes\\\":{\\\"total\\\":10,\\\"bits\\\":0,\\\"channel_points\\\":0,\\\"base\\\":10},\\\"tokens\\\":{\\\"bits\\\":0,\\\"channel_points\\\":0},\\\"total_voters\\\":10,\\\"remaining_duration_milliseconds\\\":0,\\\"top_contributor\\\":null,\\\"top_bits_contributor\\\":null,\\\"top_channel_points_contributor\\\":null}}}\"}}",
			bitsExpected:                    false,
			bitsBadgeExpected:               false,
			subscriptionExpected:            false,
			commerceExpected:                false,
			whisperExpected:                 false,
			moderationExpected:              false,
			channelPointRedemptionExpected:  false,
			videoPlaybackExpected:           false,
			channelPointEarnedExpected:      false,
			pollExpected:                    true,
			broadcastSettingsUpdateExpected: false,
			expectedReturnCode:              true,
			errorMessage:                    "Error parsing or handling poll message for poll terminated",
		},
		{
			message:                         "{\"type\":\"MESSAGE\",\"data\":{\"topic\":\"polls.252897252\",\"message\":\"{\\\"type\\\":\\\"POLL_TERMINATE\\\",\\\"data\\\":{\\\"poll\\\":{\\\"poll_id\\\":\\\"4112662a-8148-4588-9a6e-6bac924fa180\\\",\\\"owned_by\\\":\\\"252897252\\\",\\\"created_by\\\":\\\"30477280\\\",\\\"title\\\":\\\"is anthony awesome\\\",\\\"started_at\\\":\\\"2020-05-31T21:13:12.087159836Z\\\",\\\"ended_at\\\":\\\"2020-05-31T21:14:02.382129552Z\\\",\\\"ended_by\\\":\\\"30477280\\\",\\\"duration_seconds\\\":300,\\\"settings\\\":{\\\"multi_choice\\\":{\\\"is_enabled\\\":true},\\\"subscriber_only\\\":{\\\"is_enabled\\\":false},\\\"subscriber_multiplier\\\":{\\\"is_enabled\\\":false},\\\"bits_votes\\\":{\\\"is_enabled\\\":false,\\\"cost\\\":0},\\\"channel_points_votes\\\":{\\\"is_enabled\\\":false,\\\"cost\\\":0}},\\\"status\\\":\\\"TERMINATED\\\",\\\"choices\\\":[{\\\"choice_id\\\":\\\"19c8ac0e-3874-4398-9c9c-4ec7e21586a9\\\",\\\"title\\\":\\\"yes\\\",\\\"votes\\\":{\\\"total\\\":5,\\\"bits\\\":0,\\\"channel_points\\\":0,\\\"base\\\":5},\\\"tokens\\\":{\\\"bits\\\":0,\\\"channel_points\\\":0},\\\"total_voters\\\":0},{\\\"choice_id\\\":\\\"379c246d-6d7c-40c5-8a9f-47c0ba28f9f0\\\",\\\"title\\\":\\\"no\\\",\\\"votes\\\":{\\\"total\\\":5,\\\"bits\\\":0,\\\"channel_points\\\":0,\\\"base\\\":5},\\\"tokens\\\":{\\\"bits\\\":0,\\\"channel_points\\\":0},\\\"total_voters\\\":0}],\\\"votes\\\":{\\\"total\\\":10,\\\"bits\\\":0,\\\"channel_points\\\":0,\\\"base\\\":10},\\\"tokens\\\":{\\\"bits\\\":0,\\\"channel_points\\\":0},\\\"total_voters\\\":10,\\\"remaining_duration_milliseconds\\\":\\\"0\\\",\\\"top_contributor\\\":null,\\\"top_bits_contributor\\\":null,\\\"top_channel_points_contributor\\\":null}}}\"}}",
			bitsExpected:                    false,
			bitsBadgeExpected:               false,
			subscriptionExpected:            false,
			commerceExpected:                false,
			whisperExpected:                 false,
			moderationExpected:              false,
			channelPointRedemptionExpected:  false,
			videoPlaybackExpected:           false,
			channelPointEarnedExpected:      false,
			pollExpected:                    false,
			broadcastSettingsUpdateExpected: false,
			expectedReturnCode:              false,
			errorMessage:                    "Invalid poll message got handled somehow for poll terminated",
		},
		{
			message:                         "{\"type\":\"MESSAGE\",\"data\":{\"topic\":\"polls.252897252\",\"message\":\"{\\\"type\\\":\\\"POLL_ARCHIVE\\\",\\\"data\\\":{\\\"poll\\\":{\\\"poll_id\\\":\\\"4112662a-8148-4588-9a6e-6bac924fa180\\\",\\\"owned_by\\\":\\\"252897252\\\",\\\"created_by\\\":\\\"30477280\\\",\\\"title\\\":\\\"is anthony awesome\\\",\\\"started_at\\\":\\\"2020-05-31T21:13:12.087159836Z\\\",\\\"ended_at\\\":\\\"2020-05-31T21:14:02.382129552Z\\\",\\\"ended_by\\\":\\\"30477280\\\",\\\"duration_seconds\\\":300,\\\"settings\\\":{\\\"multi_choice\\\":{\\\"is_enabled\\\":true},\\\"subscriber_only\\\":{\\\"is_enabled\\\":false},\\\"subscriber_multiplier\\\":{\\\"is_enabled\\\":false},\\\"bits_votes\\\":{\\\"is_enabled\\\":false,\\\"cost\\\":0},\\\"channel_points_votes\\\":{\\\"is_enabled\\\":false,\\\"cost\\\":0}},\\\"status\\\":\\\"ARCHIVED\\\",\\\"choices\\\":[{\\\"choice_id\\\":\\\"19c8ac0e-3874-4398-9c9c-4ec7e21586a9\\\",\\\"title\\\":\\\"yes\\\",\\\"votes\\\":{\\\"total\\\":5,\\\"bits\\\":0,\\\"channel_points\\\":0,\\\"base\\\":5},\\\"tokens\\\":{\\\"bits\\\":0,\\\"channel_points\\\":0},\\\"total_voters\\\":0},{\\\"choice_id\\\":\\\"379c246d-6d7c-40c5-8a9f-47c0ba28f9f0\\\",\\\"title\\\":\\\"no\\\",\\\"votes\\\":{\\\"total\\\":5,\\\"bits\\\":0,\\\"channel_points\\\":0,\\\"base\\\":5},\\\"tokens\\\":{\\\"bits\\\":0,\\\"channel_points\\\":0},\\\"total_voters\\\":0}],\\\"votes\\\":{\\\"total\\\":10,\\\"bits\\\":0,\\\"channel_points\\\":0,\\\"base\\\":10},\\\"tokens\\\":{\\\"bits\\\":0,\\\"channel_points\\\":0},\\\"total_voters\\\":10,\\\"remaining_duration_milliseconds\\\":0,\\\"top_contributor\\\":null,\\\"top_bits_contributor\\\":null,\\\"top_channel_points_contributor\\\":null}}}\"}}",
			bitsExpected:                    false,
			bitsBadgeExpected:               false,
			subscriptionExpected:            false,
			commerceExpected:                false,
			whisperExpected:                 false,
			moderationExpected:              false,
			channelPointRedemptionExpected:  false,
			videoPlaybackExpected:           false,
			channelPointEarnedExpected:      false,
			pollExpected:                    true,
			broadcastSettingsUpdateExpected: false,
			expectedReturnCode:              true,
			errorMessage:                    "Error parsing or handling poll message for poll archived",
		},
		{
			message:                         "{\"type\":\"MESSAGE\",\"data\":{\"topic\":\"polls.252897252\",\"message\":\"{\\\"type\\\":\\\"POLL_ARCHIVE\\\",\\\"data\\\":{\\\"poll\\\":{\\\"poll_id\\\":\\\"4112662a-8148-4588-9a6e-6bac924fa180\\\",\\\"owned_by\\\":\\\"252897252\\\",\\\"created_by\\\":\\\"30477280\\\",\\\"title\\\":\\\"is anthony awesome\\\",\\\"started_at\\\":\\\"2020-05-31T21:13:12.087159836Z\\\",\\\"ended_at\\\":\\\"2020-05-31T21:14:02.382129552Z\\\",\\\"ended_by\\\":\\\"30477280\\\",\\\"duration_seconds\\\":300,\\\"settings\\\":{\\\"multi_choice\\\":{\\\"is_enabled\\\":true},\\\"subscriber_only\\\":{\\\"is_enabled\\\":false},\\\"subscriber_multiplier\\\":{\\\"is_enabled\\\":false},\\\"bits_votes\\\":{\\\"is_enabled\\\":false,\\\"cost\\\":0},\\\"channel_points_votes\\\":{\\\"is_enabled\\\":false,\\\"cost\\\":0}},\\\"status\\\":\\\"ARCHIVED\\\",\\\"choices\\\":[{\\\"choice_id\\\":\\\"19c8ac0e-3874-4398-9c9c-4ec7e21586a9\\\",\\\"title\\\":\\\"yes\\\",\\\"votes\\\":{\\\"total\\\":5,\\\"bits\\\":0,\\\"channel_points\\\":0,\\\"base\\\":5},\\\"tokens\\\":{\\\"bits\\\":0,\\\"channel_points\\\":0},\\\"total_voters\\\":0},{\\\"choice_id\\\":\\\"379c246d-6d7c-40c5-8a9f-47c0ba28f9f0\\\",\\\"title\\\":\\\"no\\\",\\\"votes\\\":{\\\"total\\\":5,\\\"bits\\\":0,\\\"channel_points\\\":0,\\\"base\\\":5},\\\"tokens\\\":{\\\"bits\\\":0,\\\"channel_points\\\":0},\\\"total_voters\\\":0}],\\\"votes\\\":{\\\"total\\\":10,\\\"bits\\\":0,\\\"channel_points\\\":0,\\\"base\\\":10},\\\"tokens\\\":{\\\"bits\\\":0,\\\"channel_points\\\":0},\\\"total_voters\\\":10,\\\"remaining_duration_milliseconds\\\":\\\"0\\\",\\\"top_contributor\\\":null,\\\"top_bits_contributor\\\":null,\\\"top_channel_points_contributor\\\":null}}}\"}}",
			bitsExpected:                    false,
			bitsBadgeExpected:               false,
			subscriptionExpected:            false,
			commerceExpected:                false,
			whisperExpected:                 false,
			moderationExpected:              false,
			channelPointRedemptionExpected:  false,
			videoPlaybackExpected:           false,
			channelPointEarnedExpected:      false,
			pollExpected:                    false,
			broadcastSettingsUpdateExpected: false,
			expectedReturnCode:              false,
			errorMessage:                    "Invalid poll message got handled somehow for poll archived",
		},
		// END poll tests
		// BEGIN broadcast settings update tests
		{
			message:                         "{\"type\":\"MESSAGE\",\"data\":{\"topic\":\"broadcast-settings-update.252897252\",\"message\":\"{\\\"channel_id\\\":\\\"252897252\\\",\\\"type\\\":\\\"broadcast_settings_update\\\",\\\"channel\\\":\\\"velostinks\\\",\\\"old_status\\\":\\\"FN30+ Customs - Maya Edition\\\",\\\"status\\\":\\\"Bot Or Not? Win Vbucks!\\\",\\\"old_game\\\":\\\"Fortnite\\\",\\\"game\\\":\\\"Fortnite\\\",\\\"old_game_id\\\":33214,\\\"game_id\\\":33214}\"}}",
			bitsExpected:                    false,
			bitsBadgeExpected:               false,
			subscriptionExpected:            false,
			commerceExpected:                false,
			whisperExpected:                 false,
			moderationExpected:              false,
			channelPointRedemptionExpected:  false,
			videoPlaybackExpected:           false,
			channelPointEarnedExpected:      false,
			pollExpected:                    false,
			broadcastSettingsUpdateExpected: true,
			expectedReturnCode:              true,
			errorMessage:                    "Error parsing or handling broadcast settings update",
		},
		{
			message:                         "{\"type\":\"MESSAGE\",\"data\":{\"topic\":\"broadcast-settings-update.252897252\",\"message\":\"{\\\"channel_id\\\":252897252,\\\"type\\\":\\\"broadcast_settings_update\\\",\\\"channel\\\":\\\"velostinks\\\",\\\"old_status\\\":\\\"FN30+ Customs - Maya Edition\\\",\\\"status\\\":\\\"Bot Or Not? Win Vbucks!\\\",\\\"old_game\\\":\\\"Fortnite\\\",\\\"game\\\":\\\"Fortnite\\\",\\\"old_game_id\\\":33214,\\\"game_id\\\":33214}\"}}",
			bitsExpected:                    false,
			bitsBadgeExpected:               false,
			subscriptionExpected:            false,
			commerceExpected:                false,
			whisperExpected:                 false,
			moderationExpected:              false,
			channelPointRedemptionExpected:  false,
			videoPlaybackExpected:           false,
			channelPointEarnedExpected:      false,
			pollExpected:                    false,
			broadcastSettingsUpdateExpected: false,
			expectedReturnCode:              false,
			errorMessage:                    "Invalid broadcast settings update message got handled somehow",
		},
		// END broadcast settings update tests
	}

	// Run the actual tests
	for _, test := range table {
		bitsReceived = false
		bitsBadgeReceived = false
		subscriptionReceived = false
		commerceReceived = false
		whisperReceived = false
		moderationReceived = false
		channelPointsRedemptionReceived = false
		videoPlaybackReceived = false
		channelPointsEarnedReceived = false
		pollReceived = false
		broadcastSettingsUpdateReceived = false

		incomingMessage := &IncomingMessage{}
		err := json.Unmarshal([]byte(test.message), incomingMessage)
		require.NoError(t, err, test.errorMessage)
		retCode := client.handleTopics(*incomingMessage)
		require.Equal(t, test.expectedReturnCode, retCode, test.errorMessage)
		require.Equal(t, test.bitsExpected, bitsReceived, test.errorMessage)
		require.Equal(t, test.bitsBadgeExpected, bitsBadgeReceived, test.errorMessage)
		require.Equal(t, test.subscriptionExpected, subscriptionReceived, test.errorMessage)
		require.Equal(t, test.commerceExpected, commerceReceived, test.errorMessage)
		require.Equal(t, test.whisperExpected, whisperReceived, test.errorMessage)
		require.Equal(t, test.moderationExpected, moderationReceived, test.errorMessage)
		require.Equal(t, test.channelPointRedemptionExpected, channelPointsRedemptionReceived, test.errorMessage)
		require.Equal(t, test.videoPlaybackExpected, videoPlaybackReceived, test.errorMessage)
		require.Equal(t, test.pollExpected, pollReceived, test.errorMessage)
		require.Equal(t, test.channelPointEarnedExpected, channelPointsEarnedReceived, test.errorMessage)
		require.Equal(t, test.broadcastSettingsUpdateExpected, broadcastSettingsUpdateReceived, test.errorMessage)
	}
}

func TestClient_handleControl(t *testing.T) {
	// Test Pong
	client := Client{}
	incomingMessage := &IncomingMessage{}
	err := json.Unmarshal([]byte("{\"type\": \"PONG\"}"), incomingMessage)
	require.NoError(t, err)

	success, err := client.handleControl(*incomingMessage)
	require.NoError(t, err)
	require.True(t, success)
	require.NotZero(t, client.lastPongTS)

	// not testing reconnect because it would try to open a websocket and error out
}

func TestClient_handleCatchAll(t *testing.T) {
	client := Client{}
	incomingMessage := &IncomingMessage{}
	handlerCalled := false
	handler := func(msg IncomingMessage) {
		handlerCalled = true
	}
	client.SetCatchAllHandler(handler)
	err := json.Unmarshal([]byte("{\"type\": \"PONG\"}"), incomingMessage)
	require.NoError(t, err)

	client.handleCatchAll(*incomingMessage)
	require.True(t, handlerCalled)
}
