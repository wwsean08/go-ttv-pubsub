package ttvclient

import (
	"encoding/json"
	"errors"
	"gitlab.com/wwsean08/go-ttv-pubsub/pkg/topic"
	"time"
)

func (c *Client) readLoop() {

	for {
		message := &IncomingMessage{}
		err := c.conn.ReadJSON(&message)

		if err != nil {
			c.log("Closed read loop due to read failure", err)

			c.reconnect()
			return
		}
		c.handleCatchAll(*message)

		if ok, err := c.handleControl(*message); ok {
			//if error, then we exit the function to shut down
			if err != nil {
				return
			}
			continue
		}

		if c.handleNonce(*message) {
			continue
		}

		if c.handleTopics(*message) {
			continue
		}

		c.handleUnknown(*message)
	}

}

func (c *Client) handleNonce(msg IncomingMessage) bool {

	if val, ok := c.inFlight[msg.Nonce]; ok {
		val <- msg
		return true
	}

	return false
}

func (c *Client) handleTopics(msg IncomingMessage) bool {
	if len(msg.Data.Topic) == 0 {
		return false
	}

	switch topic.GetType(msg.Data.Topic) {
	case topic.TypeBits:
		m := &BitsMsg{}

		err := json.Unmarshal([]byte(msg.Data.Message), &m)
		if err != nil {
			return false
		}

		if c.bitsHandler != nil {
			c.bitsHandler(*m)
		}
		return true
	case topic.TypeBitsBadgeNotification:
		m := &BitsBadgeMsg{}

		err := json.Unmarshal([]byte(msg.Data.Message), &m)
		if err != nil {
			return false
		}

		if c.bitsBadgeHandler != nil {
			c.bitsBadgeHandler(*m)
		}
		return true
	case topic.TypeSubscriptions:
		m := &SubscriptionMsg{}

		err := json.Unmarshal([]byte(msg.Data.Message), &m)
		if err != nil {
			return false
		}

		if c.subscriptionsHandler != nil {
			c.subscriptionsHandler(*m)
		}
		return true
	case topic.TypeCommerce:
		m := &CommerceMsg{}

		err := json.Unmarshal([]byte(msg.Data.Message), &m)
		if err != nil {
			return false
		}

		if c.commerceHandler != nil {
			c.commerceHandler(*m)
		}
		return true
	case topic.TypeWhispers:
		m := &WhisperMsg{}

		err := json.Unmarshal([]byte(msg.Data.Message), &m)
		if err != nil {
			return false
		}

		if c.whispersHandler != nil {
			c.whispersHandler(*m)
		}
		return true
	case topic.TypeModerationAction:
		m := &ModerationActionMsg{}

		err := json.Unmarshal([]byte(msg.Data.Message), &m)
		if err != nil {
			return false
		}

		if c.moderationHandler != nil {
			c.moderationHandler(*m)
		}
		return true
	case topic.TypeChannelPointRedemption:
		m := &ChannelPointRedemptionMsg{}

		err := json.Unmarshal([]byte(msg.Data.Message), &m)
		if err != nil {
			return false
		}

		if c.channelPointRedemptionHandler != nil {
			c.channelPointRedemptionHandler(*m)
		}
		return true
	case topic.TypeVideoPlaybackByID:
		m := &VideoPlaybackMsg{}

		err := json.Unmarshal([]byte(msg.Data.Message), &m)
		if err != nil {
			return false
		}
		m.ServerTimestamp = time.Unix(int64(m.ServerTime), 0)

		if c.videoPlaybackHandler != nil {
			c.videoPlaybackHandler(*m)
		}
		return true
	case topic.TypeChannelPointEarned:
		m := &ChannelPointEarnedMsg{}

		err := json.Unmarshal([]byte(msg.Data.Message), &m)
		if err != nil {
			return false
		}

		if c.channelPointEarnedHandler != nil {
			c.channelPointEarnedHandler(*m)
		}
		return true
	case topic.TypePolls:
		m := &PollMsg{}

		err := json.Unmarshal([]byte(msg.Data.Message), &m)
		if err != nil {
			return false
		}

		if c.pollHandler != nil {
			c.pollHandler(*m)
		}
		return true
	case topic.TypeBroadcasterSettingsUpdate:
		m := &BroadcastSettingsUpdateMsg{}

		err := json.Unmarshal([]byte(msg.Data.Message), &m)
		if err != nil {
			return false
		}

		if c.broadcastSettingsUpdateHandler != nil {
			c.broadcastSettingsUpdateHandler(*m)
		}
		return true
	default:
		return false
	}

}

func (c *Client) handleControl(msg IncomingMessage) (bool, error) {
	msgType := msg.Type

	if topic.Type(msgType) == topic.TypePong {
		c.lastPongTS = time.Now().Unix()
		return true, nil
	}

	if topic.Type(msgType) == topic.TypeReconnect {
		c.log("Reconnect msg received..")
		c.reconnect()
		return true, errors.New("shutdown")
	}

	return false, nil
}

func (c *Client) handleUnknown(msg IncomingMessage) {
	if c.unknownHandler != nil {
		c.unknownHandler(msg)
		return
	}
	c.log("Unknown message received, set client.SetUnknownHandler(handler) to handle it")
}

func (c *Client) handleCatchAll(msg IncomingMessage) {
	if c.catchAllHandler != nil {
		c.catchAllHandler(msg)
	}
}
