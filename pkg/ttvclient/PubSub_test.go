package ttvclient

import (
	"github.com/stretchr/testify/require"
	"gitlab.com/wwsean08/go-ttv-pubsub/pkg/topic"
	"testing"
)

func TestClient_removeTopics(t *testing.T) {
	// Validate that if nil is input it doesn't panic
	topics := []topic.Topic{
		topic.Topic("whispers.23456"),
		topic.Topic("Whispers.34567"),
	}
	client := Client{topics: topics}
	client.removeTopics(nil)
	require.Equal(t, topics, client.topics)

	// verify that it properly removes a topic
	topicToRemove := topic.Topic("whispers.12345")
	topics = []topic.Topic{
		topic.Topic("whispers.23456"),
		topicToRemove,
		topic.Topic("Whispers.34567"),
	}
	client.topics = topics
	client.removeTopics([]topic.Topic{topicToRemove})

	require.NotContains(t, client.topics, topicToRemove)
	require.Len(t, client.topics, 2)
}

func TestClient_mergeTopics(t *testing.T) {
	// Validate that if nil is input it doesn't panic
	topics := []topic.Topic{
		topic.Topic("whispers.23456"),
		topic.Topic("Whispers.34567"),
	}
	client := Client{topics: topics}
	client.mergeTopics(nil)
	require.Equal(t, topics, client.topics)

	// verify that it properly adds a topic
	topicToAdd := topic.Topic("whispers.12345")
	topics = []topic.Topic{
		topic.Topic("whispers.23456"),
		topic.Topic("Whispers.34567"),
	}
	client.topics = topics
	client.mergeTopics([]topic.Topic{topicToAdd})

	require.Contains(t, client.topics, topicToAdd)
	require.Len(t, client.topics, 3)

	// verify that it doesn't duplicate a topic
	topics = []topic.Topic{
		topic.Topic("whispers.23456"),
		topicToAdd,
		topic.Topic("Whispers.34567"),
	}
	client.topics = topics
	client.mergeTopics([]topic.Topic{topicToAdd})

	require.Contains(t, client.topics, topicToAdd)
	require.Len(t, client.topics, 3)
}
