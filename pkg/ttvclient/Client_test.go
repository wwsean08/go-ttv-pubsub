package ttvclient

import (
	"github.com/stretchr/testify/require"
	"testing"
)

func TestClient_Log(t *testing.T) {
	client := Client{}
	logFunctionCalled := false
	logFunc := func(v ...interface{}) {
		logFunctionCalled = true
	}
	client.SetLogFunction(logFunc)

	client.log("this is a test")
	require.True(t, logFunctionCalled)
}
