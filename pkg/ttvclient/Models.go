package ttvclient

import (
	"gitlab.com/wwsean08/go-ttv-pubsub/pkg/topic"
	"time"
)

// IncomingMessage represents an unprocessed incoming message, before the `Data.Message` field is converted to the correct message type
type IncomingMessage struct {
	Type string `json:"type"`
	Data struct {
		Message string `json:"message"`
		Topic   string `json:"topic"`
	} `json:"data"`
	Nonce string `json:"nonce"`
	Error string `json:"error"`
}

// OutgoingMessage represents an outgoing message to the server
type OutgoingMessage struct {
	Type  string `json:"type,omitempty"`
	Nonce string `json:"nonce,omitempty"`
	Data  struct {
		Topics    []topic.Topic `json:"topics,omitempty"`
		AuthToken string        `json:"auth_token,omitempty"`
	} `json:"data,omitempty"`
}

// ModerationActionMsg represents the data twitch sends for a moderator actions being taken
type ModerationActionMsg struct {
	Data struct {
		Type             string   `json:"type"`
		ModerationAction string   `json:"moderation_action"`
		Args             []string `json:"args"`
		CreatedBy        string   `json:"created_by"`
		CreatedByUserID  string   `json:"created_by_user_id"`
		MsgID            string   `json:"msg_id"`
		TargetUserID     string   `json:"target_user_id"`
		TargetUserLogin  string   `json:"target_user_login"`
	} `json:"data"`
}

// WhisperMsg represents a the data twitch sends on a whisper
type WhisperMsg struct {
	Type string `json:"type"`
	Data struct {
		ID       string `json:"id"`
		LastRead int    `json:"last_read"`
		Archived bool   `json:"archived"`
		Muted    bool   `json:"muted"`
		SpamInfo struct {
			Likelihood        string `json:"likelihood"`
			LastMarkedNotSpam int    `json:"last_marked_not_spam"`
		} `json:"spam_info"`
		WhitelistedUntil string `json:"whitelisted_until"`
	} `json:"data"`
	DataObject struct {
		ID       string `json:"id"`
		LastRead int    `json:"last_read"`
		Archived bool   `json:"archived"`
		Muted    bool   `json:"muted"`
		SpamInfo struct {
			Likelihood        string `json:"likelihood"`
			LastMarkedNotSpam int    `json:"last_marked_not_spam"`
		} `json:"spam_info"`
		WhitelistedUntil string `json:"whitelisted_until"`
	} `json:"data_object"`
}

// CommerceMsg represents the data twitch sends for commerce messages
// Deprecated: twitch has deprecated the commerce topic, and not specified a replacement, this may disappear at any time
type CommerceMsg struct {
	UserName        string `json:"user_name"`
	DisplayName     string `json:"display_name"`
	ChannelName     string `json:"channel_name"`
	UserID          string `json:"user_id"`
	ChannelID       string `json:"channel_id"`
	Time            string `json:"time"`
	ItemImageURL    string `json:"item_image_url"`
	ItemDescription string `json:"item_description"`
	SupportsChannel bool   `json:"supports_channel"`
	PurchaseMessage struct {
		Message string `json:"message"`
		Emotes  []struct {
			Start int `json:"start"`
			End   int `json:"end"`
			ID    int `json:"id"`
		} `json:"emotes"`
	} `json:"purchase_message"`
}

// SubscriptionMsg represents the data twitch sends for a subscription
type SubscriptionMsg struct {
	UserName    string `json:"user_name"`
	DisplayName string `json:"display_name"`
	ChannelName string `json:"channel_name"`
	UserID      string `json:"user_id"`
	ChannelID   string `json:"channel_id"`
	Time        string `json:"time"`
	SubPlan     string `json:"sub_plan"`
	SubPlanName string `json:"sub_plan_name"`
	Months      int    `json:"months"`
	Context     string `json:"context"`
	IsGift      bool   `json:"is_gift"`
	SubMessage  struct {
		Message string      `json:"message"`
		Emotes  interface{} `json:"emotes"`
	} `json:"sub_message"`
	RecipientID          string `json:"recipient_id"`
	RecipientUserName    string `json:"recipient_user_name"`
	RecipientDisplayName string `json:"recipient_display_name"`
	MultiMonthDuration   int    `json:"multi_month_duration"`
}

// BitsBadgeMsg represents the data that twitch sends on a bits badge event
type BitsBadgeMsg struct {
	UserID      string    `json:"user_id"`
	UserName    string    `json:"user_name"`
	ChannelID   string    `json:"channel_id"`
	ChannelName string    `json:"channel_name"`
	BadgeTier   int       `json:"badge_tier"`
	ChatMessage string    `json:"chat_message"`
	Time        time.Time `json:"time"`
}

// BitsMsg represents the data twitch sends when someone spends bits in a channel
type BitsMsg struct {
	Data struct {
		UserName         string    `json:"user_name"`
		ChannelName      string    `json:"channel_name"`
		UserID           string    `json:"user_id"`
		ChannelID        string    `json:"channel_id"`
		Time             time.Time `json:"time"`
		ChatMessage      string    `json:"chat_message"`
		BitsUsed         int       `json:"bits_used"`
		TotalBitsUsed    int       `json:"total_bits_used"`
		Context          string    `json:"context"`
		BadgeEntitlement struct {
			NewVersion      int `json:"new_version"`
			PreviousVersion int `json:"previous_version"`
		} `json:"badge_entitlement"`
	} `json:"data"`
	Version     string `json:"version"`
	MessageType string `json:"message_type"`
	MessageID   string `json:"message_id"`
	IsAnonymous bool   `json:"is_anonymous"`
}

// ChannelPointRedemptionMsg represents the data twitch sends when someone redeems channel points
type ChannelPointRedemptionMsg struct {
	Data struct {
		Timestamp  time.Time `json:"time"`
		Redemption struct {
			ID         string    `json:"id"`
			ChannelID  string    `json:"channel_id"`
			RedeemedAt time.Time `json:"redeemed_at"`
			UserInput  string    `json:"user_input"`
			Status     string    `json:"status"`
			User       struct {
				ID          string `json:"id"`
				Login       string `json:"login"`
				DisplayName string `json:"display_name"`
			}
			Reward struct {
				ID                               string `json:"id"`
				ChannelID                        string `json:"channel_id"`
				Title                            string `json:"title"`
				Prompt                           string `json:"prompt"`
				Cost                             int    `json:"cost"`
				IsUserInputRequired              bool   `json:"is_user_input_required"`
				IsSubOnly                        bool   `json:"is_sub_only"`
				BackgroundColor                  string `json:"background_color"`
				IsEnabled                        bool   `json:"is_enabled"`
				IsPaused                         bool   `json:"is_paused"`
				IsInStock                        bool   `json:"is_in_stock"`
				ShouldRedemptionSkipRequestQueue bool   `json:"should_redemption_skip_request_queue"`
				Image                            struct {
					URL1x string `json:"url_1x"`
					URL2x string `json:"url_2x"`
					URL4x string `json:"url_4x"`
				}
				DefaultImage struct {
					URL1x string `json:"url_1x"`
					URL2x string `json:"url_2x"`
					URL4x string `json:"url_4x"`
				}
				MaxPerStream struct {
					IsEnabled    bool `json:"is_enabled"`
					MaxPerStream int  `json:"max_per_stream"`
				}
			}
		}
	}
}

// VideoPlaybackMsg represents the data twitch sends for the video playback topic
type VideoPlaybackMsg struct {
	Type            string  `json:"type"`
	ServerTime      float64 `json:"server_time"`
	ServerTimestamp time.Time
	Viewers         int64 `json:"viewers"`
}

// ChannelPointEarnedMsg represents the data twitch sends for when a user earns channel points
type ChannelPointEarnedMsg struct {
	Type string `json:"type"`
	Data struct {
		Timestamp time.Time `json:"timestamp"`
		ChannelID string    `json:"channel_id"`
		PointGain struct {
			UserID         string `json:"user_id"`
			ChannelID      string `json:"channel_id"`
			TotalPoints    int64  `json:"total_points"`
			BaselinePoints int64  `json:"baseline_points"`
			ReasonCode     string `json:"reason_code"`
			Multipliers    []struct {
				ReasonCode string `json:"reason_code"`
				Factor     int64  `json:"factor"`
			} `json:"multipliers"`
		} `json:"point_gain"`
		Claim struct {
			ID        string    `json:"id"`
			UserID    string    `json:"user_id"`
			ChannelID string    `json:"channel_id"`
			CreatedAt time.Time `json:"created_at"`
			PointGain struct {
				UserID         string `json:"user_id"`
				ChannelID      string `json:"channel_id"`
				TotalPoints    int64  `json:"total_points"`
				BaselinePoints int64  `json:"baseline_points"`
				ReasonCode     string `json:"reason_code"`
				Multipliers    []struct {
					ReasonCode string `json:"reason_code"`
					Factor     int64  `json:"factor"`
				} `json:"multipliers"`
			} `json:"point_gain"`
			Balance *struct {
				UserID    string `json:"user_id"`
				ChannelID string `json:"channel_id"`
				Balance   int64  `json:"balance"`
			} `json:"balance"`
		} `json:"claim"`
	} `json:"data"`
}

// PollMsg represents the data twitch sends for updates when a poll is happening (poll created, updated, terminates, or archived)
type PollMsg struct {
	Type string `json:"type"`
	Data struct {
		Poll struct {
			PollID                        string     `json:"poll_id"`
			OwnedBy                       string     `json:"owned_by"`
			CreatedBy                     string     `json:"created_by"`
			Title                         string     `json:"title"`
			StartedAt                     time.Time  `json:"started_at"`
			EndedAt                       *time.Time `json:"ended_at"`
			EndedBy                       *string    `json:"ended_by"`
			DurationInSeconds             int64      `json:"duration_seconds"`
			Status                        string     `json:"status"`
			TotalVoters                   int64      `json:"total_voters"`
			RemainingDurationMilliseconds int64      `json:"remaining_duration_milliseconds"`
			TopContributor                *string    `json:"top_contributor"`
			TopBitsContributor            *string    `json:"top_bits_contributor"`
			TopChannelPointsContributor   *string    `json:"top_channel_points_contributor"`
			Settings                      struct {
				MultiChoice struct {
					Enabled bool `json:"is_enabled"`
				} `json:"multi_choice"`
				SubscriberOnly struct {
					Enabled bool `json:"is_enabled"`
				} `json:"subscriber_only"`
				SubscriberMultiplier struct {
					Enabled bool `json:"is_enabled"`
				} `json:"subscriber_multiplier"`
				BitsVotes struct {
					Enabled bool  `json:"is_enabled"`
					Cost    int64 `json:"cost"`
				} `json:"bits_votes"`
				ChannelPointsVotes struct {
					Enabled bool  `json:"is_enabled"`
					Cost    int64 `json:"cost"`
				}
			} `json:"settings"`
			Votes struct {
				Total         int64 `json:"total"`
				Bits          int64 `json:"bits"`
				ChannelPoints int64 `json:"channel_points"`
				Base          int64 `json:"base"`
			} `json:"votes"`
			Tokens struct {
				Bits          int64 `json:"bits"`
				ChannelPoints int64 `json:"channel_points"`
			} `json:"tokens"`
			Choices []struct {
				ChoiceID    string `json:"choice_id"`
				Title       string `json:"title"`
				TotalVoters int64  `json:"total_voters"`
				Votes       struct {
					Total         int64 `json:"total"`
					Bits          int64 `json:"bits"`
					ChannelPoints int64 `json:"channel_points"`
					Base          int64 `json:"base"`
				} `json:"votes"`
				Tokens struct {
					Bits          int64 `json:"bits"`
					ChannelPoints int64 `json:"channel_points"`
				} `json:"tokens"`
			} `json:"choices"`
		} `json:"poll"`
	} `json:"data"`
}

// BroadcastSettingsUpdateMsg represents the data twitch sends for updates to the twitch stream settings
type BroadcastSettingsUpdateMsg struct {
	ChannelID string `json:"channel_id"`
	Type      string `json:"type"`
	Channel   string `json:"channel"`
	OldStatus string `json:"old_status"`
	Status    string `json:"status"`
	OldGame   string `json:"old_game"`
	Game      string `json:"game"`
	OldGameID int64  `json:"old_game_id"`
	GameID    int64  `json:"game_id"`
}
