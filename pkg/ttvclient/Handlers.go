package ttvclient

// HandlerFunction is a generic notification handler
type HandlerFunction func(message IncomingMessage)

// BitsHandlerFunction is a function type to handle bits redemption notifications
type BitsHandlerFunction func(message BitsMsg)

// BitsBadgeHandlerFunction is a function type to handle bits badge notifications
type BitsBadgeHandlerFunction func(message BitsBadgeMsg)

// SubscriptionsHandlerFunction is a function type to handle subscription notifications
type SubscriptionsHandlerFunction func(message SubscriptionMsg)

// CommerceHandlerFunction is a function type to handle commerce notifications
// Deprecated: twitch has deprecated the commerce topic, and not specified a replacement, this may disappear at any time
type CommerceHandlerFunction func(message CommerceMsg)

// WhispersHandlerFunction is a function type to handle whisper notifications
type WhispersHandlerFunction func(message WhisperMsg)

// ModerationActionHandlerFunction is a function type to handle moderation action notifications
type ModerationActionHandlerFunction func(message ModerationActionMsg)

// ChannelPointRedemptionHandlerFunction is a function type to handle channel point redemption notifications
type ChannelPointRedemptionHandlerFunction func(message ChannelPointRedemptionMsg)

// VideoPlaybackByIDHandlerFunction is a function type to handle video playback by id notifications
type VideoPlaybackByIDHandlerFunction func(message VideoPlaybackMsg)

// ChannelPointEarnedHandlerFunction is a function type to handle channel point earned notifications
type ChannelPointEarnedHandlerFunction func(message ChannelPointEarnedMsg)

// PollHandlerFunction is a function type to handle poll notifications
type PollHandlerFunction func(message PollMsg)

// BroadcastSettingsUpdateHandlerFunction is a function type to handle broadcast settings update notifications
type BroadcastSettingsUpdateHandlerFunction func(message BroadcastSettingsUpdateMsg)

// ResultFunction is a function type returned when sending an outgoing message
type ResultFunction func() *IncomingMessage

// LogFunction is a function type to handle logging from ttvclient
type LogFunction func(...interface{})

// SetBitsHandler sets a function to handle incoming bits notifications
func (c *Client) SetBitsHandler(h BitsHandlerFunction) {
	c.bitsHandler = h
}

// SetBitsBadgeHandler sets a function to handle incoming bits badge notifications
func (c *Client) SetBitsBadgeHandler(h BitsBadgeHandlerFunction) {
	c.bitsBadgeHandler = h
}

// SetSubscriptionsHandler sets a function to handle incoming subscription notifications
func (c *Client) SetSubscriptionsHandler(h SubscriptionsHandlerFunction) {
	c.subscriptionsHandler = h
}

// SetCommerceHandler sets a function to handle incoming commerce notifications
// Deprecated: twitch has deprecated the commerce topic, and not specified a replacement, this may disappear at any time
func (c *Client) SetCommerceHandler(h CommerceHandlerFunction) {
	c.commerceHandler = h
}

// SetWhisperHandler sets a function to handle incoming whisper notifications
func (c *Client) SetWhisperHandler(h WhispersHandlerFunction) {
	c.whispersHandler = h
}

// SetModerationHandler sets a function to handle incoming moderation action notifications
func (c *Client) SetModerationHandler(h ModerationActionHandlerFunction) {
	c.moderationHandler = h
}

// SetChannelPointRedemptionHandler sets a function to handle incoming channel point redemption notifications
func (c *Client) SetChannelPointRedemptionHandler(h ChannelPointRedemptionHandlerFunction) {
	c.channelPointRedemptionHandler = h
}

// SetVideoPlaybackByIDHandler sets a function to handle incoming messages from the video playback by id topic
func (c *Client) SetVideoPlaybackByIDHandler(h VideoPlaybackByIDHandlerFunction) {
	c.videoPlaybackHandler = h
}

// SetChannelPointEarnedHandler sets a function to handle incoming channel point earned notifications
func (c *Client) SetChannelPointEarnedHandler(h ChannelPointEarnedHandlerFunction) {
	c.channelPointEarnedHandler = h
}

// SetPollHandler sets a function to handle incoming poll notifications
func (c *Client) SetPollHandler(h PollHandlerFunction) {
	c.pollHandler = h
}

// SetBroadcastSettingsUpdateHandler sets a function to handle incoming broadcast setting update notifications
func (c *Client) SetBroadcastSettingsUpdateHandler(h BroadcastSettingsUpdateHandlerFunction) {
	c.broadcastSettingsUpdateHandler = h
}

// SetCatchAllHandler sets a function to handle for all messages received by the client
func (c *Client) SetCatchAllHandler(h HandlerFunction) {
	c.catchAllHandler = h
}

// SetUnknownHandler Sets a handler to handle messages that are in an unknown format
func (c *Client) SetUnknownHandler(h HandlerFunction) {
	c.unknownHandler = h
}

// SetLogFunction sets a handler for logging messages
func (c *Client) SetLogFunction(fn LogFunction) {
	c.logFunction = fn
}
