package ttvclient

import (
	"errors"
	"gitlab.com/wwsean08/go-ttv-pubsub/pkg/topic"
	"sync/atomic"
)

// ErrorOperationFailed this error usually indicates the credentials are incorrect
var ErrorOperationFailed = errors.New("sub/unsub operation failed")

// ErrorNotConnected this error means the connection is down / ttvclient has been closed
var ErrorNotConnected = errors.New("not connected")

// Subscribe will subscribe to one or more topics when connected.
func (c *Client) Subscribe(topics []topic.Topic) error {
	if c.isConnected() == false {
		return ErrorNotConnected
	}

	resultFN, err := c.request(&OutgoingMessage{
		Type: "LISTEN",
		Data: struct {
			Topics    []topic.Topic `json:"topics,omitempty"`
			AuthToken string        `json:"auth_token,omitempty"`
		}{
			Topics:    topics,
			AuthToken: c.authToken,
		},
	})

	if err != nil {
		return err
	}

	result := resultFN()

	if len(result.Error) == 0 {
		c.mergeTopics(topics)
		return nil
	}

	return ErrorOperationFailed
}

// Unsubscribe will unsubscribe from one or more topics when connected.
func (c *Client) Unsubscribe(topics []topic.Topic) error {
	if c.isConnected() == false {
		return ErrorNotConnected
	}

	resultFN, err := c.request(&OutgoingMessage{
		Type: "UNLISTEN",
		Data: struct {
			Topics    []topic.Topic `json:"topics,omitempty"`
			AuthToken string        `json:"auth_token,omitempty"`
		}{
			Topics:    topics,
			AuthToken: c.authToken,
		},
	})

	if err != nil {
		return err
	}

	result := resultFN()

	if len(result.Error) == 0 {
		c.removeTopics(topics)
		return nil
	}

	return ErrorOperationFailed
}

func (c *Client) removeTopics(topics []topic.Topic) {
	//remove topics from list
	newList := make([]topic.Topic, 0)
	for _, t := range c.topics {
		exists := false
		for _, p := range topics {
			if t == p {
				exists = true
			}
		}

		if exists == false {
			newList = append(newList, t)
		}
	}
	c.topics = newList
}

func (c *Client) mergeTopics(topics []topic.Topic) {
	for _, t := range topics {
		exists := false
		for _, ct := range c.topics {
			if t == ct {
				exists = true
			}
		}

		if exists == false {
			c.topics = append(c.topics, t)
		}
	}
}

// Close closes the websocket connection to twitch
func (c *Client) Close() error {
	c.log("Closing websocket client..")
	atomic.StoreInt64(&c.connectionStatus, 2) //ping loop will die
	return c.conn.Close()
}
