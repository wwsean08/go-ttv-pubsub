package ttvclient

import (
	"github.com/stretchr/testify/require"
	"testing"
)

func TestClient_SetBitsHandler(t *testing.T) {
	client := &Client{}
	called := false
	testFunc := func(message BitsMsg) {
		called = true
	}
	client.SetBitsHandler(testFunc)
	client.bitsHandler(BitsMsg{})
	require.True(t, called)
}

func TestClient_SetBitsBadgeHandler(t *testing.T) {
	client := &Client{}
	called := false
	testFunc := func(message BitsBadgeMsg) {
		called = true
	}
	client.SetBitsBadgeHandler(testFunc)
	client.bitsBadgeHandler(BitsBadgeMsg{})
	require.True(t, called)
}

func TestClient_SetSubscriptionsHandler(t *testing.T) {
	client := &Client{}
	called := false
	testFunc := func(message SubscriptionMsg) {
		called = true
	}
	client.SetSubscriptionsHandler(testFunc)
	client.subscriptionsHandler(SubscriptionMsg{})
	require.True(t, called)
}

func TestClient_SetCommerceHandler(t *testing.T) {
	client := &Client{}
	called := false
	testFunc := func(message CommerceMsg) {
		called = true
	}
	client.SetCommerceHandler(testFunc)
	client.commerceHandler(CommerceMsg{})
	require.True(t, called)
}

func TestClient_SetWhisperHandler(t *testing.T) {
	client := &Client{}
	called := false
	testFunc := func(message WhisperMsg) {
		called = true
	}
	client.SetWhisperHandler(testFunc)
	client.whispersHandler(WhisperMsg{})
	require.True(t, called)
}

func TestClient_SetModerationHandler(t *testing.T) {
	client := &Client{}
	called := false
	testFunc := func(message ModerationActionMsg) {
		called = true
	}
	client.SetModerationHandler(testFunc)
	client.moderationHandler(ModerationActionMsg{})
	require.True(t, called)
}

func TestClient_SetVideoPlaybackByIDHandler(t *testing.T) {
	client := &Client{}
	called := false
	testFunc := func(_ VideoPlaybackMsg) {
		called = true
	}

	client.SetVideoPlaybackByIDHandler(testFunc)
	client.videoPlaybackHandler(VideoPlaybackMsg{})
	require.True(t, called)
}

func TestClient_SetChannelPointEarnedHandler(t *testing.T) {
	client := Client{}
	called := false
	testFunc := func(_ ChannelPointEarnedMsg) {
		called = true
	}
	client.SetChannelPointEarnedHandler(testFunc)
	client.channelPointEarnedHandler(ChannelPointEarnedMsg{})
	require.True(t, called)
}

func TestClient_SetCatchAllHandler(t *testing.T) {
	client := &Client{}
	called := false
	testFunc := func(message IncomingMessage) {
		called = true
	}
	client.SetCatchAllHandler(testFunc)
	client.catchAllHandler(IncomingMessage{})
	require.True(t, called)
}

func TestClient_SetUnknownHandler(t *testing.T) {
	client := &Client{}
	called := false
	testFunc := func(message IncomingMessage) {
		called = true
	}
	client.SetUnknownHandler(testFunc)
	client.unknownHandler(IncomingMessage{})
	require.True(t, called)
}

func TestClient_SetLogFunction(t *testing.T) {
	client := &Client{}
	called := false
	testFunc := func(...interface{}) {
		called = true
	}
	client.SetLogFunction(testFunc)
	client.logFunction("this is a test")
	require.True(t, called)
}
